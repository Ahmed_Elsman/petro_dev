//
//  LeftMenuVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/15/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import BetterSegmentedControl
import EasyLocalization
import NVActivityIndicatorView

class LeftMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var userImage: UIImageView!
    var languageSegmentControl = BetterSegmentedControl()
    var selectedIndexForLanguage: UInt = 1
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var appVersion: UILabel!
    @IBOutlet var sideMenuTable: UITableView!
    let titles: [String] = ["Home".locale, "My Account".locale, "News".locale, "My Bills".locale, "Reports".locale,"Petro Sites".locale,"Change Language".locale,"Send Complaint".locale,"Contact Us".locale,"About App".locale,"Share app".locale,"Logout".locale]
    let icon: [String] = ["home-ico", "profile-ico", "points-ico", "mybill-icon", "report-icon","locator-icon","lang","send-icon","contact-icon","about-petro","share-petro","signout-ico"]

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setup() {
        let lang =  EasyLocalization.getLanguage() == .ar ? "ar" : "en"
        if(lang == "ar"){
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        name.text = UserUtil.loadUser()?.name
        email.text = UserUtil.loadUser()?.email
        if((UserUtil.loadUser()?.image)! != ""){
            let userImageUrl = URL(string:(UserUtil.loadUser()?.image)!)
            self.userImage.af_setImage(withURL:userImageUrl!)
        }
        selectedIndexForLanguage =  EasyLocalization.getLanguage() == .en ? 1 : 0
        let currentAppVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        appVersion.text = "Information V\(currentAppVersion ?? "")"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuCell", for: indexPath) as! SideMenuCell
        cell.titleText.text = self.titles[indexPath.row]
        cell.titleImage.image = UIImage(named: icon[indexPath.row])
        cell.langView.isHidden = true
        cell.selectionStyle = .none
        if (indexPath.row == 6){
            cell.langView.isHidden = false
            
            languageSegmentControl = BetterSegmentedControl(
                frame: CGRect(x: 0, y: 0, width: 70, height: 30),
                segments: LabelSegment.segments(withTitles: ["ع", "En"],
                                                normalFont: UIFont(name: "HelveticaNeue", size: 14.0)!,
                                                normalTextColor: .lightGray,
                                                selectedFont: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!,
                                                selectedTextColor: .white),
                index: Int(selectedIndexForLanguage),
                options: [.backgroundColor(Color.newBlueColor),
                          .indicatorViewBackgroundColor(.white),
                          .cornerRadius(15)])
            languageSegmentControl.addTarget(self, action: #selector(changeLanguageButton), for: .valueChanged)
            languageSegmentControl.layer.cornerRadius = 15
            languageSegmentControl.clipsToBounds = true
            cell.langView.addSubview(languageSegmentControl)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        if(indexPath.row == 0)
        {
            let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVCID") as! HomeVC
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        if(indexPath.row == 1)
        {
            let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        if(indexPath.row == 2)
        {
            let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "MyPointsVC") as! MyPointsVC
            viewController.userType = "User"
            viewController.pageIndex = 3
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        if(indexPath.row == 3)
        {
            let viewController = UIStoryboard(name: "Bills", bundle: nil).instantiateViewController(withIdentifier: "MyBillsVC") as! MyBillsVC
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        if(indexPath.row == 4)
        {
            let viewController = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
            viewController.userType = "User"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        if(indexPath.row == 5)
        {
            let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "PetroSitesVC") as! PetroSitesVC
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        if(indexPath.row == 6){
            
        }
        if(indexPath.row == 7)
        {
            let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "SendAcomplaintVC") as! SendAcomplaintVC
            viewController.userType = "User"
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        if(indexPath.row == 8)
        {
            let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            viewController.userType = "User"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        if(indexPath.row == 9)
        {
            let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
            viewController.userType = "User"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        if(indexPath.row == 10){
            //Set the default sharing message.
            let message = "You can download app from here : "
            //Set the link to share.
            if let link = NSURL(string: "http://itunes.apple.com/app/id1267297826")
            {
                let objectsToShare = [message,link] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        if(indexPath.row == 11)
        {
            let alertController = UIAlertController(title: "Attention".locale, message: "logoutQuestion".locale, preferredStyle: .alert)
            
            let continueButton = UIAlertAction(title: "Continue".locale, style: .default) { (action:UIAlertAction) in
                print("You've pressed Continue");
                
                UserUtil.removeUser()
                let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "SelectionTypeVC") as! SelectionTypeVC
                viewController.fromLogOut = true
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
            let startOverButton = UIAlertAction(title: "Cancel".locale, style: .destructive) { (action:UIAlertAction) in
                print("You've pressed Start over");
                self.dismiss(animated: true, completion: nil)
            }
            
            alertController.addAction(continueButton)
            alertController.addAction(startOverButton)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func changeLanguageButton() {
        switchLang()
    }
    
    func switchLang()
    {
        DispatchQueue.main.async {
            let size = CGSize(width: 50, height: 50)
            NVActivityIndicatorView.DEFAULT_COLOR = Color.blueColor
            NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = .black
            self.startAnimating(size, message: "Waiting...".locale, type: NVActivityIndicatorType.ballBeat )
            
            let currentLanguage = EasyLocalization.getLanguage()
            if currentLanguage == .en {
                self.selectedIndexForLanguage = 0
                EasyLocalization.setLanguage(.ar)
                self.prepareAppFont("NeoSansArabic")
                UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
//                UIView.appearance().semanticContentAttribute = .forceRightToLeft
            }
            else
            {
                EasyLocalization.setLanguage(.en)
                UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                self.selectedIndexForLanguage = 1
                self.prepareAppFont("Helvetica")
//                UIView.appearance().semanticContentAttribute = .forceLeftToRight
            }
            UserDefaults.standard.synchronize()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                self.StopLoading()
                NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                self.goToHomeFromLogin()
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       /* if (segue.identifier == Config.Present_Petro_Sites) {
            let destinationVC = segue.destination as! PetroSitesVC
        }
         */
    }

}
