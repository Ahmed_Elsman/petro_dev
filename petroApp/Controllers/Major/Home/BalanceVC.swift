//
//  BalanceVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/21/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class BalanceVC: UIViewController {

    @IBOutlet weak var circleViewBg: UIView!
    @IBOutlet weak var addBalanceView: UIView!
    @IBOutlet weak var amount: NormalEventsTextField!
    @IBOutlet weak var passwordText: NormalEventsTextField!
    var vehicleID : Int?
    var balanceValue : Float?
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        self.circleViewBg.layer.cornerRadius = self.circleViewBg.frame.size.width/2
        self.circleViewBg.clipsToBounds = true
        UIView.animate(withDuration: 4.0) {
            self.progressView.value = CGFloat(self.balanceValue!)
        }
        addBalanceView.isHidden = true
    }

    // MARK:- delete balance
    func deleteBalanceData(){
        
        let alertController = UIAlertController(title: "Attention".locale, message: "QuestionDeleteBalance".locale, preferredStyle: .alert)
        
        let continueButton = UIAlertAction(title: "Continue".locale, style: .default) { (action:UIAlertAction) in
            print("You've pressed Continue");
            self.StartLoading()
            print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
            let param:[String: Any] = ["vehicle":self.vehicleID ?? 0]
            
            Requests.instance.request(link: "deleteBalance".route, method: .post, parameters: param) { (response) in
                print(response)
                self.StopLoading()
                if response.haveError{
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data!["success"].bool == true {
                        print(response)
                        self.view.endEditing(true)
                        DispatchQueue.main.async {
                            self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                            self.progressView.value = 0
                        }
                    }
                    else{
                        self.view.endEditing(true)
                        self.showErrorMessage(response.data!)
                    }
                }
            }
        }
        
        let startOverButton = UIAlertAction(title: "Cancel".locale, style: .destructive) { (action:UIAlertAction) in
            print("You've pressed Start over");
            self.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(continueButton)
        alertController.addAction(startOverButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- add new balance
    func addNewBalanceData(){
        StartLoading()
        let lang =  Config.locale == .ar ? "ar" : "en"
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let param:[String: Any] = ["vehicle":vehicleID ?? 0,
                                   "password":passwordText.text ?? "",
                                   "amount":amount.text ?? "",
                                   "lang":lang]
        
        Requests.instance.request(link: "addBalance".route, method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                        self.progressView.value = CGFloat(Int(self.amount.text!)!)
                        self.amount.text = ""
                        self.passwordText.text = ""
                        self.setView(view: self.addBalanceView, hidden: true)
                    }
                }
                else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }

    @IBAction func deleteBalanceBtnTouched(_ sender: UIButton) {
        setView(view: addBalanceView, hidden: true)
        deleteBalanceData()
    }
    
    @IBAction func addBalanceBtnTouched(_ sender: UIButton) {
        setView(view: addBalanceView, hidden: false)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    @IBAction func addNewBalance(_ sender: UIButton) {
        validation() ? addNewBalanceData() : nil
    }
    
    //    MARK:- validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:amount.text! ,name : "Amount".locale , type:"required"))
        fields.append(ValidationData(value:passwordText.text! ,name : "Password".locale , type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
}

