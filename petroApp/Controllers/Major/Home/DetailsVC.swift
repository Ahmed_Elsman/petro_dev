//
//  DetailsVC.swift
//  petroApp
//
//  Created by Elsman on 7/20/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import CRRefresh
import DropDown

class DetailsVC: UIViewController,UIScrollViewDelegate,VehicleCommenderVCDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var fuelViewColor: UIView!
    @IBOutlet weak var arabicNumbersLabel: UILabel!
    @IBOutlet weak var arabicLettersLabel: UILabel!
    @IBOutlet weak var englishNumbersLabel: UILabel!
    @IBOutlet weak var englishLettersLabel: UILabel!
    @IBOutlet var carNumber: UIImageView!
    @IBOutlet var carType: UILabel!
    @IBOutlet var carBrand: UILabel!
    @IBOutlet var carModel: UILabel!
    @IBOutlet var carYear: UILabel!
    @IBOutlet var carFuelType: UILabel!
    @IBOutlet var carImage: UIImageView!
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var refreshScrollView: UIScrollView!
    @IBOutlet weak var anchorView: UIView!
    var vehicle: Vehicle?
    var pageMenu : CAPSPageMenu?
    var carDetails: SingleCar?
    let moreDropDown = DropDown()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Car Details".locale
    }

    override func viewDidAppear(_ animated: Bool) {
        setup()
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    func setup() {
        // setup navigation header
        backBtn.setIcon(icon: .ionicons(.iosArrowBack), iconSize: 27, color: .white, forState: .normal)
        editBtn.setIcon(icon: .ionicons(.more), iconSize: 27, color: .white, forState: .normal)
        viewTitle.text = vehicle?.carBrand
        setupMoreDropDown()
        // setup page view data
        arabicLettersLabel.text = vehicle?.carPlateLettersAr
        englishLettersLabel.text = String((vehicle?.carPlateLettersEn as! String).reversed())
        arabicNumbersLabel.text = vehicle?.carPlateNumbersAr
        englishNumbersLabel.text = vehicle?.carPlateNumbersEn
        loadData()
        
        /// animator: your customize animator, default is NormalHeaderAnimator
        self.refreshScrollView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            /// start refresh
            self?.loadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                /// Stop refresh when your job finished, it will reset refresh footer if completion is true
                self?.refreshScrollView.cr.endHeaderRefresh()
            })
        }
        
        self.refreshScrollView.contentSize = CGSize(width:self.view.frame.width, height:self.view.frame.height)

    }
    
    func setupMoreDropDown() {
        moreDropDown.dataSource.removeAll()
        moreDropDown.dataSource.append("report".locale)
        moreDropDown.dataSource.append("modify".locale)
        moreDropDown.dataSource.append("deleteTitle".locale)
        moreDropDown.anchorView = self.anchorView
        moreDropDown.selectionAction = { [weak self] (index, item) in
            index == 0 ? self?.reportCar() : index == 1 ? self?.editCar() : index == 2 ? self?.deleteCar() : nil
        }
    }
    
    @IBAction func backBtnTouched(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func moreBtnTouched(_ sender: UIButton) {
        moreDropDown.show()
    }
    
    func deleteCommender() {
        loadData()
    }
    
    func deleteCommenderLoading() {
        self.StartLoading()
    }
    
    func deleteCommenderStopLoading() {
        self.StopLoading()
    }
    
    func setupPageMenu() {
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let balanceController =  UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "BalanceVC") as! BalanceVC
        balanceController.title = "BalanceTab".locale
        balanceController.balanceValue = carDetails?.balance
        balanceController.vehicleID = carDetails?.id
        controllerArray.append(balanceController)
        let vehicleCommenderController =  UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "VehicleCommenderVC") as! VehicleCommenderVC
        vehicleCommenderController.title = "VehicleCommenderTab".locale
        vehicleCommenderController.commenders = carDetails?.commenders
        vehicleCommenderController.delegate = self
        vehicleCommenderController.vehicleNumber = String("\(carDetails?.id ?? 0)")
        controllerArray.append(vehicleCommenderController)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorPercentageHeight(0),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .selectionIndicatorColor(Color.blueColor),
            .selectionIndicatorHeight(1),
            .viewBackgroundColor(UIColor.white),
            .scrollMenuBackgroundColor(UIColor.white),
            .selectedMenuItemLabelColor(Color.blueColor)
        ]
        
        // Initialize page menu with controller array, frame, and optional parameters
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(origin: .zero, size: CGSize(width:self.view.frame.width,height:self.view.frame.height)), pageMenuOptions: parameters)
        
        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        self.menuContainer.addSubview(pageMenu!.view)
    }
    
    // MARK:- load cars Data
    func loadData(){
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "\("getVehicle".route)\(vehicle?.id ?? 0)", method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        let car = response.data!["data"]
                        self.carDetails = SingleCar.init(car)
                        self.setupPageMenu()
                        self.carImage.af_setImage(withURL: URL(string:(self.carDetails?.vehicleImage)!)!)
                        self.carType.text = self.carDetails?.carType
                        self.carBrand.text = self.carDetails?.carBrand
                        self.carModel.text = self.carDetails?.carModel
                        self.carYear.text = "\(self.carDetails?.carYear ?? 0)"
                        self.fuelViewColor.backgroundColor = UIColor(hex:(self.carDetails?.fuelTypeColor)!)
                        self.carNumber.af_setImage(withURL: URL(string:(self.carDetails?.plateImage)!)!)
                    }
                }
                else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    // MARK:- edit car Data
    func editCar(){
        let viewController = UIStoryboard(name: "AddCar", bundle: nil).instantiateViewController(withIdentifier: "AddNewCarVC") as! AddNewCarVC
        viewController.carDetails = carDetails
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    // MARK:- report for car
    func reportCar(){
        let viewController = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
        let selectedVehicleIndexes : NSMutableArray = NSMutableArray()
        selectedVehicleIndexes.add(self.carDetails?.id ?? 0)
        viewController.selectedVehicleIndexes = selectedVehicleIndexes
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK:- delete car Data
    func deleteCar(){
        
        let alertController = UIAlertController(title: "Attention".locale, message: "QuestionCar".locale, preferredStyle: .alert)
        
        let continueButton = UIAlertAction(title: "Continue".locale, style: .default) { (action:UIAlertAction) in
            print("You've pressed Continue");
            self.StartLoading()
            print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
            let param:[String: Any] = [:]
            
            Requests.instance.request(link: "\("getVehicle".route)\(self.vehicle?.id ?? 0)", method: .delete, parameters: param) { (response) in
                print(response)
                self.StopLoading()
                if response.haveError{
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data!["success"].bool == true {
                        print(response)
                        self.view.endEditing(true)
                        self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                        self.goToHome()
                    }
                    else{
                        self.view.endEditing(true)
                        self.showErrorMessage(response.data!)
                    }
                }
            }
        }
        
        let startOverButton = UIAlertAction(title: "Cancel".locale, style: .destructive) { (action:UIAlertAction) in
            print("You've pressed Start over");
            self.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(continueButton)
        alertController.addAction(startOverButton)
        self.present(alertController, animated: true, completion: nil)

    }
}
