//
//  VehicleCommenderVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/21/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import SwipeCellKit

protocol VehicleCommenderVCDelegate {
    func deleteCommender()
    func deleteCommenderLoading()
    func deleteCommenderStopLoading()
}

class VehicleCommenderVC: UIViewController , UITableViewDelegate, UITableViewDataSource,SwipeTableViewCellDelegate {
    
    @IBOutlet var vehicleTable: UITableView!
    var delegate: VehicleCommenderVCDelegate?
    var commenders:[Commender]?
    var vehicleNumber : String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  (commenders?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleCellID", for: indexPath) as!VehicleCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.refreshBtn.tag = indexPath.row
        cell.refreshBtn.addTarget(self, action: #selector(refreshAction), for: .touchUpInside)
        cell.config(commenders![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete".locale) { action, indexPath in
            // handle action by updating model with deletion
            print("delete")
            let alertController = UIAlertController(title: "Attention".locale, message: "Question".locale, preferredStyle: .alert)
            
            let continueButton = UIAlertAction(title: "Continue".locale, style: .default) { (action:UIAlertAction) in
                print("You've pressed Continue");
                self.delegate?.deleteCommenderLoading()
                print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
                let param:[String: Any] = [:]
                
                Requests.instance.request(link: "\("editDelegate".route)/\(self.commenders![indexPath.row].id ?? 0)", method: .delete, parameters: param) { (response) in
                    print(response)
                    self.delegate?.deleteCommenderStopLoading()
                    if response.haveError{
                        self.DangerAlert(message: response.error)
                    }else{
                        if response.data!["success"].bool == true {
                            print(response)
                            self.view.endEditing(true)
                            self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                            self.delegate?.deleteCommender()
                        }
                        else{
                            self.view.endEditing(true)
                            self.showErrorMessage(response.data!)
                        }
                    }
                }
            }
            
            let startOverButton = UIAlertAction(title: "Cancel".locale, style: .destructive) { (action:UIAlertAction) in
                print("You've pressed Start over");
                self.dismiss(animated: true, completion: nil)
            }
            
            alertController.addAction(continueButton)
            alertController.addAction(startOverButton)
            self.present(alertController, animated: true, completion: nil)
            
            action.fulfill(with: .delete)
        }
        
        let editAction = SwipeAction(style: .default, title: "Edit".locale) { action, indexPath in
            print("edit")
            let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "EditDelegateInformationVC") as! EditDelegateInformationVC
            viewController.vehicleNumber = self.vehicleNumber
            viewController.delegate = self.commenders![indexPath.row]
            viewController.delegateActionType = "edit"
            self.view.window?.rootViewController?.show(viewController, sender: nil)
        }
        return [deleteAction,editAction]
    }
    
    @IBAction func addNewCommenderBtnAction(_ sender: Any) {
        let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "EditDelegateInformationVC") as! EditDelegateInformationVC
        viewController.vehicleNumber = self.vehicleNumber
        viewController.delegateActionType = "add"
        self.view.window?.rootViewController?.show(viewController, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive
        options.transitionStyle = .border
        return options
    }
    
    @objc func refreshAction(sender: UIButton!) {
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "\("regenerateCode".route)/\(String(commenders![(sender?.tag)!].id))", method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error)
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    let indexPath = IndexPath(item: (sender?.tag)!, section: 0)
                    let cell:VehicleCell = self.vehicleTable.cellForRow(at:indexPath) as! VehicleCell
                    cell.commenderID.text = "\(response.data!["data"]["code"])"
                }
                else{
                    self.RightNavBarItems(self.addBtn())
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75.0
    }

}
