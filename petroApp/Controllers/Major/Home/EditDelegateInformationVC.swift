//
//  EditDelegateInformationVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 10/14/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import ImagePicker

class EditDelegateInformationVC: UIViewController,ImagePickerDelegate {

    var delegate : Commender?
    var vehicleNumber : String?
    var delegateActionType : String?
    @IBOutlet weak var delegateImage: UIImageView!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(delegateActionType == "edit"){
            fullName.text = delegate?.name
            userName.text = delegate?.username
            mobileNumber.text = delegate?.mobile
            delegateImage.af_setImage(withURL: URL(string:(delegate?.image)!)!)
            if(delegateImage.image !=  UIImage(named: "photo-camera.png")){
                delegateImage.contentMode = .scaleAspectFit
            }
            password.isHidden = true
            topConstraint.constant = -45
        }else{
            password.isHidden = false
            topConstraint.constant = 15
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func doneBtnAction(_ sender: UIButton) {
        if(delegateActionType == "edit"){
            validation() ? editDelegate() : nil
        }else{
            validation() ? addDelegate() : nil
        }
    }
    
    //    MARK:- login validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:fullName.text! ,name : "Full Name".locale , type:"required"))
        fields.append(ValidationData(value:userName.text! ,name : "UserName".locale , type:"required|Email".locale))
        fields.append(ValidationData(value:mobileNumber.text! ,name : "Mobile".locale , type:"required"))
        if(delegateActionType != "edit"){
            fields.append(ValidationData(value:password.text! ,name : "Password".locale , type:"required"))
        }
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
    
    func editDelegate() {
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        var strBase64 : String = ""
        if((delegateImage.image != nil) && (delegateImage.image !=  UIImage(named: "photo-camera.png"))){
            let imageData:NSData = delegateImage.image!.jpegData(compressionQuality: 0.5)! as NSData
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["lang":lang,
                                   "username":userName.text ?? "",
                                   "mobile":mobileNumber.text ?? "",
                                   "name":fullName.text ?? "",
                                   "vehicle":vehicleNumber ?? "",
                                   "delegate_image":strBase64]
        
        Requests.instance.request(link: "\("editDelegate".route)/\(delegate?.id ?? 0)", method: .put, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error)
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    func addDelegate() {
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        var strBase64 : String = ""
        if((delegateImage.image != nil) && (delegateImage.image !=  UIImage(named: "photo-camera.png"))){
            let imageData:NSData = delegateImage.image!.jpegData(compressionQuality: 0.5)! as NSData
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["lang":lang,
                                   "username":userName.text ?? "",
                                   "mobile":mobileNumber.text ?? "",
                                   "name":fullName.text ?? "",
                                   "vehicle":vehicleNumber ?? "",
                                   "delegate_image":strBase64,
                                   "password":password.text ?? ""]
        
        Requests.instance.request(link: "\("editDelegate".route)", method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error)
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    @IBAction func selectImageBtnTouched(_ sender: Any) {
        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 1
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - ImagePickerDelegate
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        images.count > 0 ? delegateImage.image = images[0] : nil
        images.count > 0 ? (delegateImage.contentMode = .scaleAspectFit) : delegateImage.image == nil ? (delegateImage.contentMode = .center) : (delegateImage.contentMode = .scaleAspectFit)
    }
}
