//
//  HomeVC.swift
//  petroApp
//
//  Created by Elsman on 7/10/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import SwiftyJSON
import CRRefresh
import EasyLocalization

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var noCarsLabel: UILabel!
    @IBOutlet weak var showReportsBtn: UIButton!
    @IBOutlet var homeTableView: UITableView!
    var vehicleItems:[Vehicle]?
    var selectedVehicle: Vehicle?
    var selectedVehicleIndexes : NSMutableArray = NSMutableArray()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sendRequest()
        showReportsBtn.isHidden = true
        noCarsLabel.isHidden = true
        let lang =  EasyLocalization.getLanguage() == .ar ? "ar" : "en"
        
        if(lang == "ar"){
            RightNavBarItems(MenuBtn())
        }else{
            LeftNavBarItems(MenuBtn())
        }
        
        title = "Cars".locale
        /// animator: your customize animator, default is NormalHeaderAnimator
        self.homeTableView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            /// start refresh
            self?.sendRequest()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                /// Stop refresh when your job finished, it will reset refresh footer if completion is true
                self?.homeTableView.cr.endHeaderRefresh()
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    // MARK:- Send Request
    func sendRequest(){
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["username":UserUtil.loadUser()?.email ?? "",
                                   "password":UserUtil.loadUserPassword() ?? "",
                                   "lang":lang,
                                   "type":"8",
                                   "app_version":"1.0.3"]
        StartLoading()
        Requests.instance.request(link: "signin".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    UserUtil.saveUserToken(response.data!["data"])
                    UserUtil.saveUserPassword(UserUtil.loadUserPassword() ?? "")
                    UserUtil.saveUserType(UserUtil.loadUserType() ?? "")
                    self.loadData()
                    self.getUserData()
                }else{
                }
            }
        }
    }
    
    // MARK:- get User Data
    func getUserData(){
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "profile".route, method: .get, parameters: param) { (response) in
            print(response)
            if response.haveError{
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                }else{
                }
            }
        }
    }
    
    // MARK:- load cars Data
    func loadData(){
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "\("getVehicles".route)\(lang)", method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error)
                let lang =  EasyLocalization.getLanguage() == .ar ? "ar" : "en"
                if(lang == "ar"){
                    self.LeftNavBarItems(self.addBtn())
                }else{
                    self.RightNavBarItems(self.addBtn())
                }
                
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.StopLoading()
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        let Vehicles = response.data!["data"].map{Vehicle($0.1)}
                        self.vehicleItems = Vehicles
                        self.NumberOfCars((self.vehicleItems?.count)!, maxCount: response.data!["max_vehicle_number"].int!)
                        let lang =  EasyLocalization.getLanguage() == .ar ? "ar" : "en"
                        if(lang == "ar"){
                            self.LeftNavBarItems(self.addBtn())
                        }else{
                            self.RightNavBarItems(self.addBtn())
                        }
                        if (Vehicles.count > 0){
                            self.noCarsLabel.isHidden = true
                            self.homeTableView.isHidden = false
                        }else{
                            self.noCarsLabel.isHidden = false
                            self.homeTableView.isHidden = true
                        }
                        self.homeTableView.reloadData()
                    }
                }
                else{
                    let lang =  EasyLocalization.getLanguage() == .ar ? "ar" : "en"
                    if(lang == "ar"){
                        self.LeftNavBarItems(self.addBtn())
                    }else{
                        self.RightNavBarItems(self.addBtn())
                    }
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  vehicleItems != nil ? (vehicleItems?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeCellID", for: indexPath) as!HomeCell
        cell.selectionStyle = .none
        if selectedVehicleIndexes.contains(vehicleItems![indexPath.row].id) {
            cell.checkBoxBtn.isSelected = true
        }else{
            cell.checkBoxBtn.isSelected = false
        }
        cell.checkBoxBtn.addTarget(self, action: #selector(reportsAction), for: .touchUpInside)
        cell.checkBoxBtn.tag = indexPath.row
        cell.config(vehicleItems![indexPath.row])
        return cell
    }
    
    @objc func reportsAction(sender: UIButton!) {
        if selectedVehicleIndexes.contains(vehicleItems![sender.tag].id){
            selectedVehicleIndexes.remove(vehicleItems![sender.tag].id)
        }else {
            selectedVehicleIndexes.add(vehicleItems![sender.tag].id)
        }
        selectedVehicleIndexes.count > 0 ? (showReportsBtn.isHidden = false) : (showReportsBtn.isHidden = true)
        self.homeTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        viewController.vehicle = vehicleItems?[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func showReportsBtnTouched(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Reports", bundle: nil).instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
        viewController.selectedVehicleIndexes = selectedVehicleIndexes
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
