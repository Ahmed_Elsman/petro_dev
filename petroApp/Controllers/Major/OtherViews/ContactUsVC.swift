//
//  ContactUsVC.swift
//  petroApp
//
//  Created by Elsman on 8/15/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {

    var userType : String = ""
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var whatsAppMobile: UILabel!
    @IBOutlet weak var email: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Contact Us".locale
        userType == "User" ? LeftNavBarItems(MenuBtn()) : LeftNavBarItems(VehicleMenuBtn())
        getUserData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func callBtnTouched(_ sender: UIButton) {
        if let url = URL(string: "tel://\(self.mobileNumber.text ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func sendMailBtnTouched(_ sender: UIButton) {
        if let url = URL(string: "mailto:\(self.email.text ?? "")") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    // MARK:- settings Data
    func getUserData(){
        self.StartLoading()
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "settings".route, method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    self.mobileNumber.text = response.data!["data"]["phone"].rawString()
                    self.whatsAppMobile.text = response.data!["data"]["whatsapp_number"].rawString()
                    self.email.text = response.data!["data"]["email"].rawString()
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    @IBAction func whatsAppBtnTouched(_ sender: Any) {
        
        let urlString = "whatsapp://send?text="
        
        let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let URL = NSURL(string: urlStringEncoded!)
        
        if UIApplication.shared.canOpenURL(URL! as URL) {
            UIApplication.shared.openURL(URL! as URL)
        }
    }
    
    @IBAction func sendBtnAction(_ sender: UIButton) {
        if let url = URL(string: "mailto:\(self.email.text ?? "")") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
