//
//  PetroSitesVC.swift
//  petroApp
//
//  Created by Elsman on 7/20/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SwiftyJSON

class PetroSitesVC: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {

    var locationManager = CLLocationManager()
    var latestLocation: CLLocation?
    var petroStations:[PetroStation]?
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    @IBOutlet weak var locationMap: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "PetroSites".locale
        LeftNavBarItems(MenuBtn())
        setup()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup(){
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        locationMap.delegate = self
        locationMap.mapType = MKMapType.standard
        locationMap.showsUserLocation = true
    }
    
    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Pick the location with best (= smallest value) horizontal accuracy
        latestLocation = locations.sorted { $0.horizontalAccuracy < $1.horizontalAccuracy }.first
        getPetroLocations()
        stopUpdatingLocation()
    }
    
    //MARK: - Custom Annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = false
        } else {
            annotationView?.annotation = annotation
        }
        
        if(annotationView?.annotation is MKUserLocation){
            // use default annotation
            return nil;
        }else{
            let customPointAnnotation = annotation as! CustomPointAnnotation
            annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)
            return annotationView
        }
    }
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let petroStationAnnotation = view.annotation as! CustomPointAnnotation
        let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCalloutView
        calloutView.stationName.text = petroStationAnnotation.title
        calloutView.stationAddress.text = petroStationAnnotation.subtitle
        calloutView.detailsBtn.addTarget(self, action: #selector(callOut), for: .touchUpInside)
        calloutView.detailsArrow.image = calloutView.detailsArrow.image!.withRenderingMode(.alwaysTemplate)
        calloutView.detailsArrow.tintColor = UIColor.white
        // 3
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }
    
    // MARK - Action Handler
    @objc func callOut(sender: UIButton!) {
        let view = sender.superview as! CustomCalloutView
        for(index,selectedStation) in (petroStations?.enumerated())!{
            if(view.stationName.text == selectedStation.title){
                let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "MapVC") as! MapVC
                viewController.petroStation = petroStations?[index]
                viewController.title = "\((petroStations?[index].title)!) station"
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
//    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
//        if control == view.rightCalloutAccessoryView {
//            for(index,selectedStation) in (petroStations?.enumerated())!{
//                if(view.annotation?.title == selectedStation.title){
//                    let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "MapVC") as! MapVC
//                    viewController.petroStation = petroStations?[index]
//                    viewController.title = "\((petroStations?[index].title)!) station"
//                    self.navigationController?.pushViewController(viewController, animated: true)
//                }
//            }
//        }
//    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.stopUpdatingLocation()
        }
    }
    
    // MARK:- petro locations Data
    func getPetroLocations(){
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["lang":lang,
                                   "lat":"\(latestLocation?.coordinate.latitude ?? 0.0)",
                                   "lng":"\(latestLocation?.coordinate.longitude ?? 0.0)"]
        self.StartLoading()
        Requests.instance.request(link: "getPetroLocations".route, method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    let petroStationsItems = response.data!["data"].map{PetroStation($0.1)}
                    self.petroStations = petroStationsItems
                    // delete all previous annotations
                    let allAnnotations = self.locationMap.annotations
                    self.locationMap.removeAnnotations(allAnnotations)
                    for PetroStation in self.petroStations!{
                        self.pointAnnotation = CustomPointAnnotation()
                        PetroStation.isDiesel ? (self.pointAnnotation.pinCustomImageName = "marker-yellow") : (self.pointAnnotation.pinCustomImageName = "marker-blue")
                        self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: PetroStation.lat, longitude: PetroStation.lng)
                        self.pointAnnotation.title = PetroStation.title
                        self.pointAnnotation.subtitle = PetroStation.address
                        self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: "pin")
                        self.locationMap.addAnnotation(self.pinAnnotationView.annotation!)
                    }
                    if (self.locationMap.annotations.count == self.petroStations?.count){
                        self.locationMap.showAnnotations(self.locationMap.annotations, animated: true)
                    }
                    self.locationManager.stopUpdatingLocation()
                    
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }

    @IBAction func centerMapCamera(_ sender: Any) {
        if(locationMap.userLocation.location != nil){
            centerMapOnLocation(latestLocation!, mapView: locationMap)
        }
    }
    
    func centerMapOnLocation(_ location: CLLocation, mapView: MKMapView) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}
