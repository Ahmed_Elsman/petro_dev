//
//  SendAcomplaintVC.swift
//  petroApp
//
//  Created by Elsman on 8/14/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class SendAcomplaintVC: UIViewController {

    var userType : String = ""
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var titleComp: UITextField!
    @IBOutlet weak var details: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Send Complaint".locale
        userType == "User" ? LeftNavBarItems(MenuBtn()) : LeftNavBarItems(VehicleMenuBtn())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendBtnAction(_ sender: UIButton) {
        
        if validation(){
            self.dismissKeyboard()
            sendRequest()
        }
    }
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["name":name.text!,
                                   "email":email.text!,
                                   "title":titleComp.text!,
                                   "content":details.text!,
                                   "lang":lang,]
        
        Requests.instance.request(link: "send_complaint".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.StopLoading()
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    self.goToHome()
                }else{
                    self.StopLoading()
                    self.view.endEditing(true)
                    self.DangerAlert(message: "\(response.data!["errors"].rawString()!)")
                }
            }
        }
    }
    
    //    MARK:- sign up validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:name.text! ,name : "Name".locale , type:"required"))
        fields.append(ValidationData(value:email.text! ,name : "Email".locale , type:"required"))
        fields.append(ValidationData(value:titleComp.text! ,name : "Complaint title".locale , type:"required"))
        fields.append(ValidationData(value:details.text! ,name : "Complaint details".locale , type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
    }

}
