//
//  MyPointsVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 10/14/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class MyPointsVC: UIViewController {

    var userType : String = ""
    var pageIndex : Int = 0
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if(pageIndex == 3){
            title = "News".locale
        }else if(pageIndex == 2){
            title = "Bank Details".locale
        }else if(pageIndex == 1){
            title = "TermsTitle".locale
        }
        
        userType == "User" ? LeftNavBarItems(MenuBtn()) : userType == "Delegate" ? LeftNavBarItems(VehicleMenuBtn()) : nil
        getUserData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:- settings Data
    func getUserData(){
        self.StartLoading()
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = [:]
        var catType : String = ""
        if (userType == "User"){
            catType = "ios_individual"
        }else if (userType == "UserBank"){
            catType = "ios_individual"
        }else if (userType == "UserTerms"){
            catType = "ios_individual"
        }else{
            catType = "ios_driver"
        }
        Requests.instance.request(link: "\("pages".route)\(lang)&category=\(catType)", method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["data"].count > 0{
                    print(response)
                    self.view.endEditing(true)
                    self.webView.loadHTMLString(response.data!["data"][self.pageIndex]["page_content"].rawString()!, baseURL: nil)
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }

}
