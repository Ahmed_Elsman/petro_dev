//
//  MapVC.swift
//  petroApp
//
//  Created by Elsman on 10/8/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController,MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var stationName: UITextField!
    @IBOutlet weak var stationAddress: UITextField!
    @IBOutlet weak var stationDistance: UITextField!
    var petroStation:PetroStation?
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup(){
        mapView.delegate = self
        mapView.mapType = MKMapType.standard
        mapView.showsUserLocation = true
        
        stationName.text = petroStation?.title
        stationAddress.text = petroStation?.address
        stationDistance.text = "Distance (\(String((petroStation?.distance)!)))"
        self.pointAnnotation = CustomPointAnnotation()
        (petroStation?.isDiesel)! ? (self.pointAnnotation.pinCustomImageName = "marker-yellow") : (self.pointAnnotation.pinCustomImageName = "marker-blue")
        self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: (petroStation?.lat)!, longitude: (petroStation?.lng)!)
        self.pointAnnotation.title = petroStation?.title
        self.pointAnnotation.subtitle = petroStation?.address
        self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: "pin")
        self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
    }
    
    //MARK: - Custom Annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
        } else {
            annotationView?.annotation = annotation
        }
        if(annotationView?.annotation is MKUserLocation){
            // use default annotation
            return nil;
        }else{
            let customPointAnnotation = annotation as! CustomPointAnnotation
            annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)
            return annotationView
        }
    }
    @IBAction func openGoogleMaps(_ sender: UIButton) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(self.pointAnnotation.coordinate.latitude),\(self.pointAnnotation.coordinate.longitude)&zoom=14&views=traffic&q=\(self.pointAnnotation.coordinate.latitude),\(self.pointAnnotation.coordinate.longitude)")!, options: [:], completionHandler: nil)
        } else {
            print("Can't use comgooglemaps://")
            let coordinate = CLLocationCoordinate2DMake(self.pointAnnotation.coordinate.latitude,self.pointAnnotation.coordinate.longitude)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
            mapItem.name = petroStation?.title
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        }
    }
}
