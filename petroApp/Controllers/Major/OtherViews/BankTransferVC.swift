//
//  BankTransferVC.swift
//  petroApp
//
//  Created by Elsman on 10/8/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import ImagePicker

class BankTransferVC: UIViewController,ImagePickerDelegate,UITextFieldDelegate {

    @IBOutlet weak var detailsBtnView: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var converterName: UITextField!
    @IBOutlet weak var bankBtn: UIButton!
    @IBOutlet weak var bankBtnIcon: UIImageView!
    
    @IBOutlet weak var bankBtnAr: UIButton!
    @IBOutlet weak var bankBtnIconAr: UIImageView!
    
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var ibanNumber: UITextField!
    @IBOutlet weak var accountNumber: UITextField!
    @IBOutlet weak var benifitaryName: UITextField!
    let datePicker = UIDatePicker()
    var amountValue : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        amount.text = amountValue
        let lang =  Config.locale == .ar ? "ar" : "en"
        if(lang == "ar"){
            bankBtnAr.isHidden = false
            bankBtnIconAr.isHidden = false
            
            bankBtn.isHidden = true
            bankBtnIcon.isHidden = true
        }else{
            bankBtnAr.isHidden = true
            bankBtnIconAr.isHidden = true
            
            bankBtn.isHidden = false
            bankBtnIcon.isHidden = false
        }
        
        // Do any additional setup after loading the view.
    }

    @IBAction func sendBtnTouched(_ sender: Any) {
        validation() ? requestBalance() : nil
    }
    
    @IBAction func showBankDetails(_ sender: Any) {
        let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "MyPointsVC") as! MyPointsVC
        viewController.userType = "UserBank"
        viewController.pageIndex = 2
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    //    MARK:- validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:amount.text! ,name : "Amount".locale , type:"required"))
        fields.append(ValidationData(value:converterName.text! ,name : "Converter Name".locale , type:"required"))
        fields.append(ValidationData(value:date.text! ,name : "Date".locale , type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
    
    // MARK:- request balance
    func requestBalance(){
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        var strBase64 : String = ""
        if(selectedImage.image !=  UIImage(named: "photo-camera.png")){
            let imageData:NSData = selectedImage.image!.jpegData(compressionQuality: 0.5)! as NSData
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["lang":lang,
                                   "payment_method":"3",
                                   "amount":amount.text ?? "",
                                   "bank_name":"",
                                   "transfer_date":date.text ?? "",
                                   "transfer_person_name":converterName.text ?? "",
                                   "image":strBase64]
        
        Requests.instance.request(link: "requestBalance".route, method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error)
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                        self.goToHome()
                    }
                }
                else{
                    self.view.endEditing(true)
                    self.DangerAlert(message: "\(response.data!["errors"].rawString()!)")
                }
            }
        }
    }
    @IBAction func selectImageBtnTouched(_ sender: Any) {
        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 1
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - ImagePickerDelegate
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        images.count > 0 ? selectedImage.image = images[0] : nil
        images.count > 0 ? (selectedImage.contentMode = .scaleAspectFit) : selectedImage.image == nil ? (selectedImage.contentMode = .center) : (selectedImage.contentMode = .scaleAspectFit)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == date{
            showDatePicker()
        }
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".locale, style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".locale, style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        date.inputAccessoryView = toolbar
        date.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        date.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

}
