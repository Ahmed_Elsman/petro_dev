//
//  AboutUsVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 11/5/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    var userType : String = ""
    @IBOutlet weak var aboutUsWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "About Us".locale
        userType == "User" ? LeftNavBarItems(MenuBtn()) : LeftNavBarItems(VehicleMenuBtn())
        getUserData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:- settings Data
    func getUserData(){
        self.StartLoading()
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = [:]
        var catType : String = ""
        if (userType == "User"){
            catType = "ios_individual"
        }else{
            catType = "ios_driver"
        }
        Requests.instance.request(link: "\("pages".route)\(lang)&category=\(catType)", method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["data"].count > 0{
                    print(response)
                    self.view.endEditing(true)
                    self.aboutUsWebView.loadHTMLString(response.data!["data"][0]["page_content"].rawString()!, baseURL: nil)
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }

}
