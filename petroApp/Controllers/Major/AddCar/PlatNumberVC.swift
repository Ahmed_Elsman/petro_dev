//
//  PlatNumberVC.swift
//  petroApp
//
//  Created by Elsman on 8/12/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class PlatNumberVC: UIViewController,UITextFieldDelegate,KeyboardTextFieldDelegate {
    
    @IBOutlet weak var number1TextEN: KeyboardTextField!
    @IBOutlet weak var number2TextEN: KeyboardTextField!
    @IBOutlet weak var number3TextEN: KeyboardTextField!
    @IBOutlet weak var number4TextEN: KeyboardTextField!
    @IBOutlet weak var number1TextAR: UITextField!
    @IBOutlet weak var number2TextAR: UITextField!
    @IBOutlet weak var number3TextAR: UITextField!
    @IBOutlet weak var number4TextAR: UITextField!
    @IBOutlet weak var char1TextEN: KeyboardTextField!
    @IBOutlet weak var char2TextEN: KeyboardTextField!
    @IBOutlet weak var char3TextEN: KeyboardTextField!
    @IBOutlet weak var char1TextAR: UITextField!
    @IBOutlet weak var char2TextAR: UITextField!
    @IBOutlet weak var char3TextAR: UITextField!
    @IBOutlet var carImage: UIImageView!
    
    var carTypeID : String?
    var brandID : String?
    var modelID : String?
    var year : String?
    var typeOfFuleID : String?
    var selectedImage :UIImage?
    var carDetails: SingleCar?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Plat Number".locale
        number1TextEN.keyboardDelegate = self
        number2TextEN.keyboardDelegate = self
        number3TextEN.keyboardDelegate = self
        number4TextEN.keyboardDelegate = self
        char1TextEN.keyboardDelegate = self
        char2TextEN.keyboardDelegate = self
        char3TextEN.keyboardDelegate = self
        number1TextEN.becomeFirstResponder()
        if carDetails != nil {
            let carPlateLettersAr = carDetails?.carPlateLettersAr
            self.char1TextAR.text = (carPlateLettersAr! as NSString).substring(with: NSMakeRange(4, 1))
            self.char2TextAR.text = (carPlateLettersAr! as NSString).substring(with: NSMakeRange(2, 1))
            self.char3TextAR.text = (carPlateLettersAr! as NSString).substring(with: NSMakeRange(0, 1))
            
            let carPlateLettersEn = String((carDetails?.carPlateLettersEn as! String).reversed())
            self.char1TextEN.text = (carPlateLettersEn as NSString).substring(with: NSMakeRange(0, 1))
            self.char2TextEN.text = (carPlateLettersEn as NSString).substring(with: NSMakeRange(2, 1))
            self.char3TextEN.text = (carPlateLettersEn as NSString).substring(with: NSMakeRange(4, 1))
            
            let carPlateNumbersAr = carDetails?.carPlateNumbersAr
            self.number1TextAR.text = (carPlateNumbersAr! as NSString).substring(with: NSMakeRange(6, 1))
            self.number2TextAR.text = (carPlateNumbersAr! as NSString).substring(with: NSMakeRange(4, 1))
            self.number3TextAR.text = (carPlateNumbersAr! as NSString).substring(with: NSMakeRange(2, 1))
            self.number4TextAR.text = (carPlateNumbersAr! as NSString).substring(with: NSMakeRange(0, 1))
            
            let carPlateNumbersEn = carDetails?.carPlateNumbersEn
            self.number1TextEN.text = (carPlateNumbersEn! as NSString).substring(with: NSMakeRange(0, 1))
            self.number2TextEN.text = (carPlateNumbersEn! as NSString).substring(with: NSMakeRange(2, 1))
            self.number3TextEN.text = (carPlateNumbersEn! as NSString).substring(with: NSMakeRange(4, 1))
            self.number4TextEN.text = (carPlateNumbersEn! as NSString).substring(with: NSMakeRange(6, 1))
            
            self.carImage.af_setImage(withURL: URL(string:(carDetails?.plateImage)!)!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func nextBtnTouched(_ sender: UIButton) {
        if(number1TextEN.text == "" || number2TextEN.text == "" || number3TextEN.text == "" || number4TextEN.text == "" || char1TextEN.text == "" || char2TextEN.text == "" || char3TextEN.text == ""){
            self.DangerAlert(message: "EnterPlatNumberAlert".locale)
        }else{
            checkPlatNumbers()
        }
    }

    func textFieldDidDelete(textField: UITextField) {
       print("Deleted")
        if(textField.text?.length == 0){
            if(textField == char3TextEN){
                char3TextAR.text = ""
                char2TextEN.becomeFirstResponder()
            }
            if(textField == char2TextEN){
                char2TextAR.text = ""
                char1TextEN.becomeFirstResponder()
            }
            if(textField == char1TextEN){
                char1TextAR.text = ""
                number4TextEN.becomeFirstResponder()
            }
            if(textField == number4TextEN){
                number4TextAR.text = ""
                number3TextEN.becomeFirstResponder()
            }
            if(textField == number3TextEN){
                number3TextAR.text = ""
                number2TextEN.becomeFirstResponder()
            }
            if(textField == number2TextEN){
                number2TextAR.text = ""
                number1TextEN.becomeFirstResponder()
            }
            if(textField == number1TextEN){
                number1TextAR.text = ""
                number1TextEN.becomeFirstResponder()
            }
        }else{
            if(textField == char3TextEN){
                char3TextAR.text = ""
            }
            if(textField == char2TextEN){
                char2TextAR.text = ""
            }
            if(textField == char1TextEN){
                char1TextAR.text = ""
            }
            if(textField == number4TextEN){
                number4TextAR.text = ""
            }
            if(textField == number3TextEN){
                number3TextAR.text = ""
            }
            if(textField == number2TextEN){
                number2TextAR.text = ""
            }
            if(textField == number1TextEN){
                number1TextAR.text = ""
            }
        }
    }
    
    func textField(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if number1TextEN == textField{
                if number1TextEN.text?.length == 0 {
                    if(updatedText.replacedEnglishDigitsWithArabic == "false"){
                        self.DangerAlert(message:"keyboardHint".locale)
                        number1TextEN.keyboardDelegate = self
                        number1TextEN.text = ""
                        number1TextAR.text = ""
                        number1TextEN.becomeFirstResponder()
                    }else{
                        number2TextEN.keyboardDelegate = self
                        number1TextEN.text = updatedText
                        number1TextAR.text = updatedText.replacedEnglishDigitsWithArabic
                        number2TextEN.becomeFirstResponder()
                    }
                }
            }
            if number2TextEN == textField{
                if number2TextEN.text?.length == 0 {
                    if(updatedText.replacedEnglishDigitsWithArabic == "false"){
                        self.DangerAlert(message:"keyboardHint".locale)
                        number2TextEN.keyboardDelegate = self
                        number2TextEN.text = ""
                        number2TextAR.text = ""
                        number2TextEN.becomeFirstResponder()
                    }else{
                        number3TextEN.keyboardDelegate = self
                        number2TextEN.text = updatedText
                        number2TextAR.text = updatedText.replacedEnglishDigitsWithArabic
                        number3TextEN.becomeFirstResponder()
                    }
                }
            }
            if number3TextEN == textField{
                if number3TextEN.text?.length == 0 {
                    if(updatedText.replacedEnglishDigitsWithArabic == "false"){
                        self.DangerAlert(message:"keyboardHint".locale)
                        number3TextEN.keyboardDelegate = self
                        number3TextEN.text = ""
                        number3TextAR.text = ""
                        number3TextEN.becomeFirstResponder()
                    }else{
                        number4TextEN.keyboardDelegate = self
                        number3TextEN.text = updatedText
                        number3TextAR.text = updatedText.replacedEnglishDigitsWithArabic
                        number4TextEN.becomeFirstResponder()
                    }
                }
            }
            if number4TextEN == textField{
                if number4TextEN.text?.length == 0 {
                    if(updatedText.replacedEnglishDigitsWithArabic == "false"){
                        self.DangerAlert(message:"keyboardHint".locale)
                        number4TextEN.keyboardDelegate = self
                        number4TextEN.text = ""
                        number4TextAR.text = ""
                        number4TextEN.becomeFirstResponder()
                    }else{
                        char1TextEN.keyboardDelegate = self
                        number4TextEN.text = updatedText
                        number4TextAR.text = updatedText.replacedEnglishDigitsWithArabic
                        char1TextEN.becomeFirstResponder()
                    }
                }
            }
            if char1TextEN == textField{
                if char1TextEN.text?.length == 0 {
                    if(updatedText.replacedEnglishDigitsWithArabic == "false"){
                        self.DangerAlert(message:"keyboardHintChar".locale)
                        char1TextEN.keyboardDelegate = self
                        char1TextEN.text = ""
                        char1TextAR.text = ""
                        char1TextEN.becomeFirstResponder()
                    }else{
                        char2TextEN.keyboardDelegate = self
                        char1TextEN.text = updatedText
                        char1TextAR.text = updatedText.replacedEnglishDigitsWithArabic
                        char2TextEN.becomeFirstResponder()
                    }
                }
            }
            if char2TextEN == textField{
                if char2TextEN.text?.length == 0 {
                    if(updatedText.replacedEnglishDigitsWithArabic == "false"){
                        self.DangerAlert(message:"keyboardHintChar".locale)
                        char2TextEN.keyboardDelegate = self
                        char2TextEN.text = ""
                        char2TextAR.text = ""
                        char2TextEN.becomeFirstResponder()
                    }else{
                        char3TextEN.keyboardDelegate = self
                        char2TextEN.text = updatedText
                        char2TextAR.text = updatedText.replacedEnglishDigitsWithArabic
                        char3TextEN.becomeFirstResponder()
                    }
                    
                }
            }
            if char3TextEN == textField{
                if char3TextEN.text?.length == 0 {
                    if(updatedText.replacedEnglishDigitsWithArabic == "false"){
                        self.DangerAlert(message:"keyboardHintChar".locale)
                        char3TextEN.keyboardDelegate = self
                        char3TextEN.text = ""
                        char3TextAR.text = ""
                        char3TextEN.becomeFirstResponder()
                    }else{
                        char3TextEN.keyboardDelegate = self
                        char3TextEN.text = updatedText
                        char3TextAR.text = updatedText.replacedEnglishDigitsWithArabic
                        char3TextEN.endEditing(true)
//                        checkPlatNumbers()
                    }
                }
            }
        }
        let newLenght = (textField.text?.length)! + string.length - range.length
        if(newLenght == 0){
            return true
        }else{
            return (newLenght > 1 && newLenght == 2) ? false : false
        }
    }

    // MARK:- check car plat number validation
    func checkPlatNumbers(){
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["step":"1",
                                   "lang":lang,
                                   "car_plate_letters_ar":"\(char1TextAR.text!)\(char2TextAR.text!)\(char3TextAR.text!)",
                                   "car_plate_letters_en":"\(char1TextEN.text!)\(char2TextEN.text!)\(char3TextEN.text!)",
                                   "car_plate_numbers_ar":"\(number1TextAR.text!)\(number2TextAR.text!)\(number3TextAR.text!)\(number4TextAR.text!)",
                                   "car_plate_numbers_en":"\(number1TextEN.text!)\(number2TextEN.text!)\(number3TextEN.text!)\(number4TextEN.text!)",
                                   "car_type":carTypeID!,
                                   "car_brand":brandID!,
                                   "car_model":modelID!,
                                   "car_year":year!,
                                   "fuel_type":typeOfFuleID!,
                                   "car_image":"",
                                   "trusted":""]
        Requests.instance.request(link: "\("checkVehicles".route)", method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        let viewController = UIStoryboard(name: "AddCar", bundle: nil).instantiateViewController(withIdentifier: "BasicInformationVC") as! BasicInformationVC
                        if(self.carDetails != nil){
                            viewController.carDetails = self.carDetails
                        }
                        viewController.carTypeID = "\(self.carTypeID!)"
                        viewController.brandID = "\(self.brandID!)"
                        viewController.modelID = "\(self.modelID!)"
                        viewController.year = "\(self.year!)"
                        viewController.typeOfFuleID = "\(self.typeOfFuleID!)"
                        viewController.car_plate_letters_ar = "\(self.char1TextAR.text!) \(self.char2TextAR.text!) \(self.char3TextAR.text!)"
                        viewController.car_plate_letters_en = "\(self.char1TextEN.text!) \(self.char2TextEN.text!) \(self.char3TextEN.text!)"
                        viewController.car_plate_numbers_ar = "\(self.number1TextAR.text!) \(self.number2TextAR.text!) \(self.number3TextAR.text!) \(self.number4TextAR.text!)"
                        viewController.car_plate_numbers_en = "\(self.number1TextEN.text!) \(self.number2TextEN.text!) \(self.number3TextEN.text!) \(self.number4TextEN.text!)"
                        self.selectedImage != nil ? viewController.selectedImage = self.selectedImage : nil
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
                else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
}

public extension String {
    public var replacedEnglishDigitsWithArabic: String {
        var arabicString = self
        let available_characters : NSMutableArray = ["0","1","2","3","4","5","6","7","8","9","a","b","d","e","g","h","j","k","l","n","r","s","t","u","v","x","z"]
        if(available_characters.contains(arabicString)){
            let map = ["0": "٠",
                       "1": "١",
                       "2": "٢",
                       "3": "٣",
                       "4": "٤",
                       "5": "٥",
                       "6": "٦",
                       "7": "٧",
                       "8": "٨",
                       "9": "٩",
                       "a": "أ",
                       "b": "ب",
                       "d": "د",
                       "e": "ع",
                       "g": "ق",
                       "h": "ھ",
                       "j": "ح",
                       "k": "ك",
                       "l": "ل‬",
                       "n": "ن",
                       "r": "ر‬",
                       "s": "س",
                       "t": "ط",
                       "u": "و",
                       "v": "ي",
                       "x": "ص",
                       "z": "م"]
            
            map.forEach { arabicString = arabicString.replacingOccurrences(of: $0, with: $1) }
            return arabicString
        }else{
            return "false"
        }
    }
}

// custom textFields in Keyboard
protocol KeyboardTextFieldDelegate {
    func textFieldDidDelete(textField:UITextField)
}

class KeyboardTextField: UITextField {
    
    var keyboardDelegate: KeyboardTextFieldDelegate?
    
    override func deleteBackward() {
        keyboardDelegate?.textFieldDidDelete(textField:self)
        super.deleteBackward()
    }
    
}
