//
//  AddNewCarVC.swift
//  petroApp
//
//  Created by Elsman on 8/10/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown
import ImagePicker

class AddNewCarVC: UIViewController,UITextFieldDelegate,ImagePickerDelegate {
    
    //MARK: - Properties
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var carTypetextBox: UITextField!
    @IBOutlet weak var brandTextBox: UITextField!
    @IBOutlet weak var modelTextBox: UITextField!
    @IBOutlet weak var typeOfFuleTexBox: UITextField!
    @IBOutlet weak var yearTextBox: UITextField!
    @IBOutlet weak var anchorView: UIView!
    
    //MARK: - DropDown's
    let carTypeDropDown = DropDown()
    var carTypeID : Int = 0
    let brandDropDown = DropDown()
    var brandID : Int = 0
    let modelDropDown = DropDown()
    var modelID : Int = 0
    let yearDropDown = DropDown()
    let typeOfFuleDropDown = DropDown()
    var typeOfFuleID : Int = 0
    
    var fuelTypesItems:[FuelType]?
    var vehicleBrandsItems:[VehicleBrand]?
    var modelsItems:[FuelType]?
    var vehicleTypesItems:[VehicleType]?
    
    var carDetails: SingleCar?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        loadData()
        setupYearDropDown()
        if(carDetails != nil){
            title = "EditCar".locale
            // car type
            self.carTypetextBox.text = carDetails?.carType
            self.carTypeID = (carDetails?.carTypeId)!
            // car brand and model
            self.brandTextBox.text = carDetails?.carBrand
            self.brandID = (carDetails?.carBrandId)!
            self.modelTextBox.text = carDetails?.carModel
            self.modelID = (carDetails?.carModelId)!
            // car year
            self.yearTextBox.text = String((carDetails?.carYear)!)
            // car type of fuel
            self.typeOfFuleTexBox.text = carDetails?.fuelType
            self.typeOfFuleID = (carDetails?.fuelTypeId)!
            // car image
            selectedImage.af_setImage(withURL: URL(string:(carDetails?.vehicleImage)!)!)
            if(selectedImage.image !=  UIImage(named: "photo-camera.png")){
                selectedImage.contentMode = .scaleAspectFit
            }
        }else{
            title = "AddNewCar".locale
        }
    }
    
    func setupCarTypeDropDown(){
        for VehicleType in vehicleTypesItems! {
            carTypeDropDown.dataSource.append(VehicleType.title)
        }
        carTypeDropDown.anchorView = carTypetextBox
        carTypeDropDown.bottomOffset = CGPoint(x: 0, y: carTypetextBox.bounds.height)
        carTypeDropDown.direction = .any
        // Action triggered on selection
        carTypeDropDown.selectionAction = { [weak self] (index, item) in
            self?.carTypetextBox.text = item
            self?.carTypeID = (self?.vehicleTypesItems![index].id)!
        }
    }
    
    func setupBrandDropDown() {
        for VehicleBrand in vehicleBrandsItems! {
            brandDropDown.dataSource.append(VehicleBrand.title)
        }
        brandDropDown.anchorView = self.anchorView
        brandDropDown.selectionAction = { [weak self] (index, item) in
            self?.brandTextBox.text = item
            self?.modelsItems?.removeAll()
            self?.modelTextBox.text = ""
            self?.modelsItems = self?.vehicleBrandsItems![index].models
            self?.setupModelDropDown()
            self?.brandID = (self?.vehicleBrandsItems![index].id)!
        }
    }
    
    func setupModelDropDown() {
        modelDropDown.dataSource.removeAll()
        for FuelType in modelsItems! {
            modelDropDown.dataSource.append(FuelType.title)
        }
        modelDropDown.anchorView = self.anchorView
        modelDropDown.selectionAction = { [weak self] (index, item) in
            self?.modelTextBox.text = item
            self?.modelID = (self?.modelsItems![index].id)!
        }
    }
    
    func setupYearDropDown() {
        for year in stride(from: 2018, to: 1950, by: -1){
            yearDropDown.dataSource.append("\(year)")
        }
        yearDropDown.anchorView = self.anchorView
        yearDropDown.selectionAction = { [weak self] (index, item) in
            self?.yearTextBox.text = item
        }
    }
    
    func setupTypeOfFuleDropDown(){
        for FuelType in fuelTypesItems! {
            typeOfFuleDropDown.dataSource.append(FuelType.title)
        }
        typeOfFuleDropDown.anchorView = typeOfFuleTexBox
        typeOfFuleDropDown.bottomOffset = CGPoint(x: 0, y: typeOfFuleTexBox.bounds.height)
        typeOfFuleDropDown.direction = .any
        // Action triggered on selection
        typeOfFuleDropDown.selectionAction = { [weak self] (index, item) in
            self?.typeOfFuleTexBox.text = item
            self?.typeOfFuleID = (self?.fuelTypesItems![index].id)!
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.endEditing(true)
        textField.tintColor = .clear
        if textField == self.carTypetextBox {
            carTypeDropDown.show()
        }else if textField == self.brandTextBox {
            brandDropDown.show()
        }else if (textField == self.modelTextBox && modelsItems != nil) {
            modelDropDown.show()
        }else if textField == self.yearTextBox {
            yearDropDown.show()
        }else if textField == self.typeOfFuleTexBox {
            typeOfFuleDropDown.show()
        }
        
    }
    
    // MARK:- load models
    func loadData(){
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "\("getModels".route)?lang=\(lang)", method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        let FuelTypes = response.data!["data"]["fuel_types"].map{FuelType($0.1)}
                        let VehicleBrands = response.data!["data"]["vehicle_brands"].map{VehicleBrand($0.1)}
                        let VehicleTypes = response.data!["data"]["vehicle_types"].map{VehicleType($0.1)}
                        self.fuelTypesItems = FuelTypes
                        self.vehicleBrandsItems = VehicleBrands
                        self.vehicleTypesItems = VehicleTypes
                        self.setupBrandDropDown()
                        self.setupTypeOfFuleDropDown()
                        self.setupCarTypeDropDown()
                    }
                }
                else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    @IBAction func NextBtnTouched(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "AddCar", bundle: nil).instantiateViewController(withIdentifier: "PlatNumberVC") as! PlatNumberVC
        viewController.carTypeID = "\(carTypeID)"
        viewController.brandID = "\(brandID)"
        viewController.modelID = "\(modelID)"
        viewController.year = self.yearTextBox.text
        viewController.typeOfFuleID = "\(typeOfFuleID)"
        if(carDetails != nil){
            viewController.carDetails = carDetails
        }
        selectedImage.image !=  UIImage(named: "photo-camera.png") ? viewController.selectedImage = selectedImage.image : nil
        carTypeID == 0 ? self.DangerAlert(message:"EnterCarType".locale) : brandID == 0 ? self.DangerAlert(message:"EnterBrandType".locale) : modelID == 0 ? self.DangerAlert(message:"EnterModelType".locale) : self.yearTextBox.text == "" ? self.DangerAlert(message:"EnterYear".locale) : typeOfFuleID == 0 ? self.DangerAlert(message:"EnterTypeOfFule".locale) : self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func selectImageBtnTouched(_ sender: Any) {
        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 1
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - ImagePickerDelegate
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        images.count > 0 ? selectedImage.image = images[0] : nil
        images.count > 0 ? (selectedImage.contentMode = .scaleAspectFit) : selectedImage.image == nil ? (selectedImage.contentMode = .center) : (selectedImage.contentMode = .scaleAspectFit)
    }
}
