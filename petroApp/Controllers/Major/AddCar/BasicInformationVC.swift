//
//  BasicInformationVC.swift
//  petroApp
//
//  Created by Elsman on 8/12/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import ImagePicker

class BasicInformationVC: UIViewController,ImagePickerDelegate {

    var car_plate_letters_ar : String?
    var car_plate_letters_en : String?
    var car_plate_numbers_ar : String?
    var car_plate_numbers_en : String?
    var carTypeID : String?
    var brandID : String?
    var modelID : String?
    var year : String?
    var typeOfFuleID : String?
    var selectedImage: UIImage?
    var carDetails: SingleCar?
    @IBOutlet weak var delegateImage: UIImageView!
    @IBOutlet weak var unTrustedUserForm: UIView!
    @IBOutlet weak var trusted: UISwitch!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Basic Information".locale
        if carDetails != nil {
            fullName.text = carDetails?.commenders[0].name
            userName.text = carDetails?.commenders[0].username
            mobileNumber.text = carDetails?.commenders[0].mobile
            delegateImage.af_setImage(withURL: URL(string:(carDetails?.commenders[0].image)!)!)
            if(delegateImage.image !=  UIImage(named: "photo-camera.png")){
                delegateImage.contentMode = .scaleAspectFit
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- edit car plat number
    func editCar(){
        StartLoading()
        var strBase64 : String = ""
        if(selectedImage != nil){
            let imageData:NSData = selectedImage!.jpegData(compressionQuality: 0.5)! as NSData
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        var strBase64Delegate : String = ""
        if((delegateImage.image != nil) && (delegateImage.image !=  UIImage(named: "photo-camera.png"))){
            let imageData:NSData = delegateImage.image!.jpegData(compressionQuality: 0.5)! as NSData
            strBase64Delegate = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        var param:[String: Any] = [:]
        let paramNotTursted:[String: Any] = ["step":"2",
                                             "lang":lang,
                                             "car_plate_letters_ar":car_plate_letters_ar!,
                                             "car_plate_letters_en":car_plate_letters_en!,
                                             "car_plate_numbers_ar":car_plate_numbers_ar!,
                                             "car_plate_numbers_en":car_plate_numbers_en!,
                                             "car_type":carTypeID!,
                                             "car_brand":brandID!,
                                             "car_model":modelID!,
                                             "car_year":year!,
                                             "fuel_type":typeOfFuleID!,
                                             "car_image":strBase64,
                                             "trusted":"0",
                                             "username":userName.text!,
                                             "password":password.text!,
                                             "name":fullName.text!,
                                             "mobile":mobileNumber.text!,
                                             "delegate_image":strBase64Delegate]
        
        let paramTrusted:[String: Any] = ["step":"2",
                                          "lang":lang,
                                          "car_plate_letters_ar":car_plate_letters_ar!,
                                          "car_plate_letters_en":car_plate_letters_en!,
                                          "car_plate_numbers_ar":car_plate_numbers_ar!,
                                          "car_plate_numbers_en":car_plate_numbers_en!,
                                          "car_type":carTypeID!,
                                          "car_brand":brandID!,
                                          "car_model":modelID!,
                                          "car_year":year!,
                                          "fuel_type":typeOfFuleID!,
                                          "car_image":strBase64,
                                          "trusted":"1",
                                          "username":UserUtil.loadUser()?.username ?? "",
                                          "password":UserUtil.loadUserPassword()!,
                                          "name":UserUtil.loadUser()?.name ?? "",
                                          "mobile":UserUtil.loadUser()?.mobile ?? "",
                                          "delegate_image":strBase64Delegate]
        trusted.isOn ? (param = paramTrusted) : (param = paramNotTursted)
        Requests.instance.request(link: "\("checkVehicles".route)/\(carDetails?.id ?? 0))", method: .put, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.data!["success"].bool == false {
                self.DangerAlert(message: "\(response.data!["message"].rawString()!)")
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    self.view.endEditing(true)
                    self.goToHome()
                }
                else{
                    self.view.endEditing(true)
                    self.DangerAlert(message: "\(response.data!["message"].rawString()!)")
                }
            }
        }
    }
    
    // MARK:- add new car plat number
    func checkPlatNumbers(){
        StartLoading()
        var strBase64 : String = ""
        if(selectedImage != nil){
            let imageData:NSData = selectedImage!.jpegData(compressionQuality: 0.5)! as NSData
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        var strBase64Delegate : String = ""
        if((delegateImage.image != nil) && (delegateImage.image !=  UIImage(named: "photo-camera.png"))){
            let imageData:NSData = delegateImage.image!.jpegData(compressionQuality: 0.5)! as NSData
            strBase64Delegate = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        var param:[String: Any] = [:]
        let paramNotTursted:[String: Any] = ["step":"2",
                                   "lang":lang,
                                   "car_plate_letters_ar":car_plate_letters_ar!,
                                   "car_plate_letters_en":car_plate_letters_en!,
                                   "car_plate_numbers_ar":car_plate_numbers_ar!,
                                   "car_plate_numbers_en":car_plate_numbers_en!,
                                   "car_type":carTypeID!,
                                   "car_brand":brandID!,
                                   "car_model":modelID!,
                                   "car_year":year!,
                                   "fuel_type":typeOfFuleID!,
                                   "car_image":strBase64,
                                   "trusted":"0",
                                   "username":userName.text!,
                                   "password":password.text!,
                                   "name":fullName.text!,
                                   "mobile":mobileNumber.text!,
                                   "delegate_image":strBase64Delegate]
        
        let paramTrusted:[String: Any] = ["step":"2",
                                   "lang":lang,
                                   "car_plate_letters_ar":car_plate_letters_ar!,
                                   "car_plate_letters_en":car_plate_letters_en!,
                                   "car_plate_numbers_ar":car_plate_numbers_ar!,
                                   "car_plate_numbers_en":car_plate_numbers_en!,
                                   "car_type":carTypeID!,
                                   "car_brand":brandID!,
                                   "car_model":modelID!,
                                   "car_year":year!,
                                   "fuel_type":typeOfFuleID!,
                                   "car_image":strBase64,
                                   "trusted":"1",
                                   "username":UserUtil.loadUser()?.username ?? "",
                                   "password":UserUtil.loadUserPassword()!,
                                   "name":UserUtil.loadUser()?.name ?? "",
                                   "mobile":UserUtil.loadUser()?.mobile ?? "",
                                   "delegate_image":strBase64Delegate]
        trusted.isOn ? (param = paramTrusted) : (param = paramNotTursted)
        Requests.instance.request(link: "\("checkVehicles".route)", method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.data!["success"].bool == false {
                self.DangerAlert(message: "\(response.data!["message"].rawString()!)")
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    self.view.endEditing(true)
                    self.goToHome()
                }
                else{
                    self.view.endEditing(true)
                    self.DangerAlert(message: "\(response.data!["message"].rawString()!)")
                }
            }
        }
    }
    @IBAction func sendBtnTouched(_ sender: UIButton) {
        if (carDetails != nil) {
            trusted.isOn ? editCar() : (fullName.text == "") ? self.DangerAlert(message:"EnterFullName".locale) : (userName.text == "") ? self.DangerAlert(message:"EnterUserName".locale) : (password.text == "") ? self.DangerAlert(message:"EnterPassword".locale) : (mobileNumber.text == "") ?
                self.DangerAlert(message:"EnterMobileNumber".locale) : editCar()
        }else{
            trusted.isOn ? checkPlatNumbers() : (fullName.text == "") ? self.DangerAlert(message:"EnterFullName".locale) : (userName.text == "") ? self.DangerAlert(message:"EnterUserName".locale) : (password.text == "") ? self.DangerAlert(message:"EnterPassword".locale) : (mobileNumber.text == "") ?
                self.DangerAlert(message:"EnterMobileNumber".locale) : checkPlatNumbers()
        }
        
    }
    
    @IBAction func becomeTrusted(_ sender: Any) {
        trusted.isOn ? setView(view: unTrustedUserForm, hidden: true) : setView(view: unTrustedUserForm, hidden: false)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    @IBAction func selectImageBtnTouched(_ sender: Any) {
        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 1
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - ImagePickerDelegate
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        images.count > 0 ? delegateImage.image = images[0] : nil
        images.count > 0 ? (delegateImage.contentMode = .scaleAspectFit) : delegateImage.image == nil ? (delegateImage.contentMode = .center) : (delegateImage.contentMode = .scaleAspectFit)
    }
}
