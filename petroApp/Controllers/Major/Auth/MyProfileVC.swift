//
//  MyProfileVC.swift
//  petroApp
//
//  Created by Elsman on 8/13/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import MapKit

class MyProfileVC: UIViewController,MKMapViewDelegate {

    
    @IBOutlet weak var amountText: NormalEventsTextField!
    @IBOutlet var PopUpView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var conversionBtn: UIButton!
    @IBOutlet weak var cashBtn: UIButton!
    var balanceType : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        LeftNavBarItems(MenuBtn())
        title = "My Profile".locale
        self.PopUpView.isHidden = true
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup(){
        // config view for user details
        getUserData()
        conversionBtn.setIcon(icon: .ionicons(.iosCircleOutline), iconSize: 27, color: Color.newBlueColor, forState: .normal)
        conversionBtn.setIcon(icon: .ionicons(.iosCircleFilled), iconSize: 27, color: Color.newBlueColor, forState: .selected)
        
        cashBtn.setIcon(icon: .ionicons(.iosCircleOutline), iconSize: 27, color: Color.newBlueColor, forState: .normal)
        cashBtn.setIcon(icon: .ionicons(.iosCircleFilled), iconSize: 27, color: Color.newBlueColor, forState: .selected)
    }

    @IBAction func editMyProfileBtnAction(_ sender: Any) {
        let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func modifyPasswordBtnTouched(_ sender: Any) {
        let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "EditPasswordVc") as! EditPasswordVc
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    @IBAction func openPopUp(_ sender: UIButton)
    {
        setView(view: self.PopUpView, hidden: false)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    @IBAction func addBalanceBtnTouched(_ sender: UIButton) {
        if balanceType == "visa"{
            self.DangerAlert(message: "soon".locale)
        }else if balanceType == "cash"{
            validation() ? requestBalance() : nil
        }else if balanceType == "sadad"{
            self.DangerAlert(message: "soon".locale)
        }else if balanceType == "conversion"{
            if validation(){
                let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "BankTransferVC") as! BankTransferVC
                viewController.title = "Request Balance".locale
                viewController.amountValue = amountText.text!
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }else{
            self.DangerAlert(message: "enterValidBalanceType".locale)
        }
    }
    
    //    MARK:- validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:amountText.text! ,name : "Amount".locale , type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
    }
    
    // MARK:- request balance
    func requestBalance(){
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["lang":lang,
                                   "payment_method":"3",
                                   "amount":amountText.text ?? "",
                                   "bank_name":"",
                                   "transfer_date":"",
                                   "transfer_person_name":"",
                                   "image":""]
        
        Requests.instance.request(link: "requestBalance".route, method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error)
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                        self.goToHome(true)
                    }
                }
                else{
                    self.view.endEditing(true)
                    self.DangerAlert(message: "\(response.data!["errors"].rawString()!)")
                }
            }
        }
    }
    
    @IBAction func cacellationPopUp(_ sender: UIButton)
    {
        balanceType = ""
        conversionBtn.isSelected = false
        cashBtn.isSelected = false
        setView(view: self.PopUpView, hidden: true)
    }
    // MARK:- get User Data
    func getUserData(){
        self.StartLoading()
        let param:[String: Any] = [:]
        Requests.instance.request(link: "profile".route, method: .get, parameters: param) { (response) in
            print(response)
            
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                    if((UserUtil.loadUser()?.image)! != ""){
                        let userImageUrl = URL(string:(UserUtil.loadUser()?.image)!)
                        self.userImage.af_setImage(withURL:userImageUrl!)
                    }
                    self.name.text = UserUtil.loadUser()?.name
                    self.balance.text = UserUtil.loadUser()?.balance
                    self.userName.text = UserUtil.loadUser()?.username
                    self.email.text = UserUtil.loadUser()?.email
                    self.mobileNumber.text = UserUtil.loadUser()?.mobile
                    self.address.text = UserUtil.loadUser()?.address
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = CLLocationCoordinate2D(latitude: Double((UserUtil.loadUser()?.lat)!), longitude: Double((UserUtil.loadUser()?.lng)!))
                    self.mapView.addAnnotation(annotation)
                    let viewRegion = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 100, longitudinalMeters: 100)
                    self.mapView.setRegion(viewRegion, animated: true)
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
            self.StopLoading()
        }
    }
    @IBAction func conversationBtnTouched(_ sender: Any) {
        balanceType = "conversion"
        conversionBtn.isSelected = true
        cashBtn.isSelected = false
    }
    @IBAction func cashBtnTouched(_ sender: Any) {
        balanceType = "cash"
        conversionBtn.isSelected = false
        cashBtn.isSelected = true
    }
}
