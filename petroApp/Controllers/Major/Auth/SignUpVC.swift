//
//  SignUpVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/2/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import EasyLocalization

class SignUpVC: UIViewController {

    var signUpStep : String = "1"
    var verficationCode : String = ""
    var checkBoxSelected : Bool = false
    @IBOutlet var registerBtn: UIButton!
    @IBOutlet var fristName: NormalEventsTextField!
    @IBOutlet var userName: NormalEventsTextField!
    @IBOutlet var email: NormalEventsTextField!
    @IBOutlet var mobile: NormalEventsTextField!
    @IBOutlet var password: NormalEventsTextField!
    @IBOutlet var confirmPassword: NormalEventsTextField!
    @IBOutlet weak var checkBox: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let currentLanguage = EasyLocalization.getLanguage()
        if currentLanguage == .en {
            self.checkBox.semanticContentAttribute = .forceRightToLeft
        }
        else
        {
            self.checkBox.semanticContentAttribute = .forceLeftToRight
        }
        setup()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Setup
    func setup(){
        UserUtil.removeUser()
        checkBox.setIcon(icon: .ionicons(.iosCheckmarkOutline), iconSize: 27, color: .black, forState: .normal)
        checkBox.setIcon(icon: .ionicons(.iosCheckmark), iconSize: 27, color: Color.newBlueColor, forState: .selected)
        title = "Sign Up".locale
        
    }
    
    // MARK: - Actions
    @IBAction func signUpAction(_ sender: Any) {
        
        if validation(){
            self.dismissKeyboard()
            if (((mobile.text?.length)! > 9) || ((mobile.text?.length)! < 9) || !((mobile.text?.hasPrefix("5"))!))
            {
                self.DangerAlert(message:"Please enter valid mobile number ex: 5xxxxxxxx".locale)
            }else{
                password.text! == confirmPassword.text! ? checkBoxSelected == true ? sendRequest() : self.DangerAlert(message:"please accept terms and conditions".locale) : self.DangerAlert(message:"please enter valid password match".locale)
            }
            
        }
    }
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["name":fristName.text!,
                                   "email":email.text!,
                                   "password":password.text!,
                                   "password_confirmation":confirmPassword.text!,
                                   "lang":lang,
                                   "username":userName.text!,
                                   "mobile":mobile.text!,
                                   "verification_code":verficationCode,
                                   "lat":"30,000000",
                                   "lng":"30,000000",
                                   "step":signUpStep]
        
        Requests.instance.request(link: "signup".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.StopLoading()
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    if (self.signUpStep == "1") {
                        self.signUpStep = "2"
                        self.sendRequest()
                    } else if (self.signUpStep == "2") {
                        self.StopLoading()
                        self.signUpStep = "3"
                        let newParams:[String: Any] = ["name":self.fristName.text!,
                                                       "email":self.email.text!,
                                                       "password":self.password.text!,
                                                       "password_confirmation":self.confirmPassword.text!,
                                                   "lang":lang,
                                                   "username":self.userName.text!,
                                                   "mobile":self.mobile.text!,
                                                   "verification_code":"\(response.data!["data"]["activation_code"])",
                                                   "lat":"30,000000",
                                                   "lng":"30,000000",
                                                   "step":"3"]
                        
                        let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "VerficationVCID") as! VerficationVC
                        viewController.signUpStep = "3"
                        viewController.verficationCode = "\(response.data!["data"]["activation_code"])"
                        viewController.param = newParams
                        viewController.password = self.password.text!
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }else if (self.signUpStep == "3") {
                        UserUtil.saveUserToken(response.data!["data"])
                        UserUtil.saveUserPassword(self.password.text!)
                        self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                        self.getUserData()
                    }
                }else{
                    self.StopLoading()
                    self.view.endEditing(true)
                    self.DangerAlert(message: "\(response.data!["message"].rawString()!)")
                }
            }
        }
    }
    
    // MARK:- get User Data
    func getUserData(){
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "profile".route, method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                    self.goToHome(true)
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    //    MARK:- sign up validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:fristName.text! ,name : "FristName".locale , type:"required"))
        fields.append(ValidationData(value:userName.text! ,name : "UserName".locale , type:"required"))
        fields.append(ValidationData(value:email.text! ,name : "Email".locale , type:"required|email"))
        fields.append(ValidationData(value:mobile.text! ,name : "Mobile".locale , type:"required"))
        //|password|min:6
        fields.append(ValidationData(value:password.text! ,name : "Password".locale, type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
    }
    
    @IBAction func showTermsBtnTouched(_ sender: Any) {
        let viewController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "MyPointsVC") as! MyPointsVC
        viewController.userType = "UserTerms"
        viewController.pageIndex = 1
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func checkBoxSelected(_ sender: UIButton) {
        checkBox.isSelected ? (checkBoxSelected = false) : (checkBoxSelected = true)
        checkBox.isSelected ? (checkBox.isSelected = false) : (checkBox.isSelected = true)
    }
    
}
