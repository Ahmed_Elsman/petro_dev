//
//  AddNewPasswordVC.swift
//  petroApp
//
//  Created by Elsman on 9/28/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class AddNewPasswordVC: UIViewController {

    var mobileNumber : String?
    @IBOutlet weak var code: NormalEventsTextField!
    @IBOutlet weak var newPassword: NormalEventsTextField!
    @IBOutlet weak var matchNewPassword: NormalEventsTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "PasswordRecovery".locale

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    @IBAction func resetBtnTouched(_ sender: UIButton) {
        if validation(){
            self.dismissKeyboard()
            newPassword.text == matchNewPassword.text ? sendRequest() : self.DangerAlert(message:"please enter matched new password".locale)
        }
    }
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        let param:[String: Any] = ["code":code.text ?? "",
                                   "mobile":mobileNumber ?? "",
                                   "password":newPassword.text ?? "",
                                   "confirm_password":matchNewPassword.text ?? ""]
        
        Requests.instance.request(link: "updatePassword".route, method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            self.view.endEditing(true)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "LoginMajorUserVC") as! LoginMajorUserVC
                    viewController.fromLogOut = true
                    self.navigationController?.pushViewController(viewController, animated: true)
                }else{
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    //    MARK:- login validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:code.text! ,name : "Code".locale , type:"required"))
        fields.append(ValidationData(value:newPassword.text! ,name : "New password".locale , type:"required"))
        fields.append(ValidationData(value:matchNewPassword.text! ,name : "Re-Enter New Password".locale , type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
}
