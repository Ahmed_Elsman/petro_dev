//
//  VerficationVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 2/13/19.
//  Copyright © 2019 Elsman. All rights reserved.
//

import UIKit

class VerficationVC: UIViewController {

    var param:[String: Any] = [:]
    var signUpStep : String = ""
    var verficationCode : String = ""
    var password : String = ""
    @IBOutlet weak var code: NormalEventsTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func sendActivationCode(_ sender: Any) {
        if(code.text == self.verficationCode){
            // MARK:- Send Activation Step
            StartLoading()
            Requests.instance.request(link: "signup".route, method: .post, parameters: param) { (response) in
                print(response)
                if response.haveError{
                    self.StopLoading()
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data!["success"].bool == true {
                        print(response)
                        self.view.endEditing(true)
                        if (self.signUpStep == "3") {
                            UserUtil.saveUserToken(response.data!["data"])
                            UserUtil.saveUserPassword(self.password)
                            self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                            self.getUserData()
                        }
                    }else{
                        self.StopLoading()
                        self.view.endEditing(true)
                        self.DangerAlert(message: "\(response.data!["message"].rawString()!)")
                    }
                }
            }
        }else{
            self.DangerAlert(message: "ActivationCodeError".locale)
        }
    }
    // MARK:- get User Data
    func getUserData(){
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "profile".route, method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                    self.goToHome(true)
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }

}
