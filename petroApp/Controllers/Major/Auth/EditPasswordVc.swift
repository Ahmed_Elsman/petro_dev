//
//  EditPasswordVc.swift
//  petroApp
//
//  Created by Elsman on 9/28/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class EditPasswordVc: UIViewController {

    @IBOutlet weak var matchNewPassword: NormalEventsTextField!
    @IBOutlet weak var newPassword: NormalEventsTextField!
    @IBOutlet weak var oldPassword: NormalEventsTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "EditPass".locale

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Actions
    @IBAction func changePasswordAction(_ sender: Any) {
        
        if validation(){
            self.dismissKeyboard()
            oldPassword.text == UserUtil.loadUserPassword() ? newPassword.text == matchNewPassword.text ? sendRequest() : self.DangerAlert(message:"please enter matched new password".locale) : self.DangerAlert(message:"please enter true old password".locale)
        }
    }
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["old_password":oldPassword.text ?? "",
                                   "new_password":newPassword.text ?? "",
                                   "lang":lang,
                                   "password_confirmation":matchNewPassword.text ?? ""]
        
        Requests.instance.request(link: "changePassword".route, method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            self.view.endEditing(true)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    UserUtil.saveUserPassword(self.newPassword.text!)
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    self.goToHome()
                }else{
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }

    @IBAction func forgetPasswordBtnTouched(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //    MARK:- login validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:oldPassword.text! ,name : "old password".locale , type:"required"))
        fields.append(ValidationData(value:newPassword.text! ,name : "new password".locale , type:"required"))
        fields.append(ValidationData(value:matchNewPassword.text! ,name : "Re - Enter new password".locale , type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
}
