//
//  ViewController.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/2/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class SelectionTypeVC: UIViewController {

    var fromLogOut : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        fromLogOut ? (self.navigationItem.leftBarButtonItem = backButton) : nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func vehicleBtnTouched(_ sender: Any) {
        let viewController = UIStoryboard(name: "VehicleAuth", bundle: nil).instantiateViewController(withIdentifier: "LoginVehicleUserVC") as! LoginVehicleUserVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

