//
//  ForgetPasswordVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/2/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class ForgetPasswordVC: UIViewController {

    @IBOutlet var phoneTxt: NormalEventsTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Forgot Pasword".locale
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func sendActivationCode(_ sender: Any) {
        if validation(){
            self.dismissKeyboard()
            ((phoneTxt.text?.length)! > 9) || ((phoneTxt.text?.length)! < 9) || !((phoneTxt.text?.hasPrefix("5"))!) ? self.DangerAlert(message:"Please enter valid mobile number ex: 5xxxxxxxx".locale) : sendRequest()
        }
    }
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        let param:[String: Any] = ["mobile":phoneTxt.text ?? ""]
        
        Requests.instance.request(link: "sendActivation".route, method: .post, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            self.view.endEditing(true)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "AddNewPasswordVC") as! AddNewPasswordVC
                    viewController.mobileNumber = self.phoneTxt.text
                    self.navigationController?.pushViewController(viewController, animated: true)
                }else{
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    //    MARK:- login validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:phoneTxt.text! ,name : "Phone".locale , type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
    
}
protocol PinTexFieldDelegate : UITextFieldDelegate {
    func didPressBackspace(textField : EventsActiveTextField)
}
