//
//  LanguageSelectVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/3/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import EasyLocalization

class LanguageSelectVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func selectArLang(_ sender: Any) {
        EasyLocalization.setLanguage(.ar)
        self.prepareAppFont("NeoSansArabic")
        UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
//        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        UserDefaults.standard.synchronize()
        let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "SelectionTypeVC") as! SelectionTypeVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func selectEnLang(_ sender: Any) {
        EasyLocalization.setLanguage(.en)
        self.prepareAppFont("Helvetica")
        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
//        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        UserDefaults.standard.synchronize()
        let viewController = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "SelectionTypeVC") as! SelectionTypeVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    /*func switchLang()
    {
        DispatchQueue.main.async {
            let size = CGSize(width: 50, height: 50)
            NVActivityIndicatorView.DEFAULT_COLOR = Color.blueColor
            NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = .black
            self.startAnimating(size, message: "Waiting...".locale, type: NVActivityIndicatorType.ballBeat )
            
            let currentLanguage = EasyLocalization.getLanguage()
            if currentLanguage == .en {
                self.selectedIndexForLanguage = 0
                EasyLocalization.setLanguage(.ar)
                self.prepareAppFont("NeoSansArabic")
                UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
            }else{
                EasyLocalization.setLanguage(.en)
                UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                self.selectedIndexForLanguage = 1
                self.prepareAppFont("Helvetica")
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
            }
            UserDefaults.standard.synchronize()
            
            if (UserUtil.loadUser() != nil) {
                let lang = EasyLocalization.getLanguage() == .en ? "en" : "ar"
                Requests.instance.request(link: "language".route, method: .post , parameters: ["language":lang, "api_token":UserUtil.loadUser()?.api_token ?? ""]) { (resonse) in
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        self.StopLoading()
                        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                        self.goToHome(true)
                    }
                }
                
            }else{
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    self.StopLoading()
                    NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                    self.goToHome(true)
                }
            }
        }
    }*/
}
