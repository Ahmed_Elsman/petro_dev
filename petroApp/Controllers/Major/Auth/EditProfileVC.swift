//
//  EditProfileVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 9/23/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import ImagePicker

class EditProfileVC: UIViewController,ImagePickerDelegate {

    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet var fristName: NormalEventsTextField!
    @IBOutlet var userName: NormalEventsTextField!
    @IBOutlet var email: NormalEventsTextField!
    @IBOutlet var mobile: NormalEventsTextField!
    @IBOutlet var password: NormalEventsTextField!
    @IBOutlet var confirmPassword: NormalEventsTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup
    func setup(){
        title = "Edit Profile".locale
        fristName.text = UserUtil.loadUser()?.name
        userName.text = UserUtil.loadUser()?.username
        email.text = UserUtil.loadUser()?.email
        mobile.text = UserUtil.loadUser()?.mobile
        password.text = UserUtil.loadUserPassword()
        confirmPassword.text = UserUtil.loadUserPassword()
        if((UserUtil.loadUser()?.image)! != ""){
            selectedImage.af_setImage(withURL: URL(string:(UserUtil.loadUser()?.image)!)!)
        }
        
        if(selectedImage.image !=  UIImage(named: "photo-camera.png")){
            selectedImage.contentMode = .scaleAspectFit
        }
    }
    
    @IBAction func editProfileBtnTouched(_ sender: UIButton) {
        if validation(){
            self.dismissKeyboard()
            if (((mobile.text?.length)! > 9) || ((mobile.text?.length)! < 9) || !((mobile.text?.hasPrefix("5"))!))
            {
                self.DangerAlert(message:"Please enter valid mobile number ex: 5xxxxxxxx".locale)
            }else{
                password.text! == confirmPassword.text! ? sendRequest() : self.DangerAlert(message:"please enter valid password match".locale)
            }
        }
    }
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        var strBase64 : String = ""
        if(selectedImage != nil){
            let imageData:NSData = selectedImage.image!.jpegData(compressionQuality: 0.5)! as NSData
            strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        }
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["name":fristName.text!,
                                   "email":email.text!,
                                   "password":password.text!,
                                   "password_confirmation":confirmPassword.text!,
                                   "lang":lang,
                                   "username":userName.text!,
                                   "primaryuser_image":strBase64,
                                   "mobile":mobile.text!,
                                   "lat":"30,000000",
                                   "lng":"30,000000"]
        
        Requests.instance.request(link: "editProfile".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.StopLoading()
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    UserUtil.saveUserPassword(self.password.text!)
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    self.getUserData()
                }else{
                    self.StopLoading()
                    self.view.endEditing(true)
                    self.DangerAlert(message: "\(response.data!["message"].rawString()!)")
                }
            }
        }
    }
    
    // MARK:- get User Data
    func getUserData(){
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "profile".route, method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                    self.goToHome()
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    //    MARK:- sign up validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:fristName.text! ,name : "FristName".locale , type:"required"))
        fields.append(ValidationData(value:userName.text! ,name : "UserName".locale , type:"required"))
        fields.append(ValidationData(value:email.text! ,name : "Email".locale , type:"required|email"))
        fields.append(ValidationData(value:mobile.text! ,name : "Mobile".locale , type:"required"))
        //|password|min:6
        fields.append(ValidationData(value:password.text! ,name : "Password".locale, type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
    }
    
    @IBAction func selectImageBtnTouched(_ sender: Any) {
        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 1
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - ImagePickerDelegate
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        images.count > 0 ? selectedImage.image = images[0] : nil
        images.count > 0 ? (selectedImage.contentMode = .scaleAspectFit) : selectedImage.image == nil ? (selectedImage.contentMode = .center) : (selectedImage.contentMode = .scaleAspectFit)
    }

}
