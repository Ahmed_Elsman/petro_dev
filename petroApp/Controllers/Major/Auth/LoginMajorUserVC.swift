//
//  LoginMajorUserVC.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/2/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class LoginMajorUserVC: UIViewController {

    var fromLogOut : Bool = false
    @IBOutlet var emailText: EventsTextField!
    @IBOutlet var passwordTxt: EventsTextField!
    
    @IBOutlet weak var registerBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        fromLogOut ? (self.navigationItem.leftBarButtonItem = backButton) : nil
    }
    
    // MARK: - Setup
    func setup(){
        UserUtil.removeUser()
        title = "Login".locale
        emailText.shadowColor = UIColor.white
        emailText.setIcon(emailText.icon ?? UIImage() ,CGRect(x:5,y:5,width:30,height:30))
        passwordTxt.shadowColor = UIColor.white
        passwordTxt.setIcon(passwordTxt.icon ?? UIImage() ,CGRect(x:5,y:5,width:24,height:32))
    }
    
    // MARK: - Actions
    @IBAction func loginAction(_ sender: Any) {
        
        if validation(){
            self.dismissKeyboard()
            if let number = Int(emailText.text!){
                if (((emailText.text?.length)! > 9) || ((emailText.text?.length)! < 9) || !((emailText.text?.hasPrefix("5"))!))
                {
                    self.DangerAlert(message:"Please enter valid mobile number ex: 5xxxxxxxx".locale)
                }else{
                    sendRequest()
                }
            }else{
                sendRequest()
            }
        }
    }
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        
        let lang =  Config.locale == .ar ? "ar" : "en"
        
        let param:[String: Any] = ["username":emailText.text!,
                                   "password":passwordTxt.text! ,
                                   "lang":lang,
                                   "type":"8",
                                  "app_version":"1.0.3"]
 
        // test data
//        let param:[String: Any] = ["username":"murad99",
//                                   "password":"Aa123456",
//                                   "lang":lang,
//                                   "type":"8",
//                                   "app_version":"1.0.3"]
        
        Requests.instance.request(link: "signin".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.StopLoading()
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    UserUtil.saveUserToken(response.data!["data"])
                    UserUtil.saveUserPassword(self.passwordTxt.text!)
                    UserUtil.saveUserType("User")
                    self.SuccessAlert(message: "\(response.data!["message"].rawString()!)")
                    self.getUserData()
                }else{
                    self.StopLoading()
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    // MARK:- get User Data
    func getUserData(){
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "profile".route, method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                    self.goToHome(true)
                }else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
    //    MARK:- login validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:emailText.text! ,name : "Email".locale , type:"required"))
        //|password|min:6
        fields.append(ValidationData(value:passwordTxt.text! ,name : "Password".locale, type:"required"))
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
    
}
