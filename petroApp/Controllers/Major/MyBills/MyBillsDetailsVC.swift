//
//  MyBillsDetailsVC.swift
//  petroApp
//
//  Created by Elsman on 8/13/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import MapKit
import Lightbox

class MyBillsDetailsVC: UIViewController,MKMapViewDelegate {

    let annotation = MKPointAnnotation()
    var bill: BillData?
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var fuelpumpImage: UIImageView!
    @IBOutlet weak var billScrollView: UIScrollView!
    @IBOutlet weak var vehiclePlateImage: UIImageView!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleModelName: UILabel!
    @IBOutlet weak var arabicNumbersLabel: UILabel!
    @IBOutlet weak var arabicLettersLabel: UILabel!
    @IBOutlet weak var englishNumbersLabel: UILabel!
    @IBOutlet weak var englishLettersLabel: UILabel!
    @IBOutlet weak var billDate: UILabel!
    @IBOutlet weak var billTime: UILabel!
    @IBOutlet weak var operatingNumber: UILabel!
    @IBOutlet weak var numberOfLitters: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var paymentMethod: UILabel!
    @IBOutlet weak var locationMap: MKMapView!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var commender: UILabel!
    @IBOutlet weak var fuelType: UILabel!
    @IBOutlet weak var fuelColor: UIView!
    @IBOutlet var carImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "My Bills Details".locale
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup(){
        self.billScrollView.contentSize = CGSize(width:self.view.frame.width, height:self.view.frame.height)
        vehicleImage.af_setImage(withURL: URL(string:(bill?.delegate.vehicle.vehicleImage)!)!)
        vehicleType.text = bill?.delegate.vehicle.carType
        fuelType.text = bill?.delegate.vehicle.fuelType
        self.fuelColor.backgroundColor = UIColor(hex:(bill?.delegate.vehicle.fuelTypeColor)!)
        commender.text =  bill?.delegate.name
        code.text =  bill?.delegate.code
        vehicleName.text = bill?.delegate.vehicle.carBrand
        vehicleModelName.text = bill?.delegate.vehicle.carModel
        carImage.af_setImage(withURL: URL(string:(bill?.delegate.vehicle.plateImage)!)!)
        let created = bill?.created
        billDate.text = created?.substring(9)
        billTime.text = String((created?.prefix(8))!)
        arabicLettersLabel.text = bill?.delegate.vehicle.carPlateLettersAr
        englishLettersLabel.text = String((bill?.delegate.vehicle.carPlateLettersEn as! String).reversed())
        arabicNumbersLabel.text = bill?.delegate.vehicle.carPlateNumbersAr
        englishNumbersLabel.text = bill?.delegate.vehicle.carPlateNumbersEn
        operatingNumber.text = "\(bill?.id ?? 0)"
        numberOfLitters.text = "\(bill?.litters ?? 0)"
        (bill?.vehiclePlateImage != "") ? vehiclePlateImage.af_setImage(withURL: URL(string:(bill?.vehiclePlateImage)!)!) : nil
        (bill?.fuelPumpImage != "") ? fuelpumpImage.af_setImage(withURL: URL(string:(bill?.fuelPumpImage)!)!) : nil
        price.text = "\(bill?.cost ?? 0) SAR"
        paymentMethod.text = bill?.paymentMethod
        self.pointAnnotation = CustomPointAnnotation()
        (bill?.delegate.vehicle.fuelType == "91") || (bill?.delegate.vehicle.fuelType == "92") ? (self.pointAnnotation.pinCustomImageName = "marker-blue") : (self.pointAnnotation.pinCustomImageName = "marker-yellow")
        self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double((bill?.lat)!), longitude: Double((bill?.lng)!))
        self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: "pin")
        self.locationMap.addAnnotation(self.pinAnnotationView.annotation!)
        locationMap.showAnnotations(locationMap.annotations, animated: true)
    }

    //MARK: - Custom Annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
            let calloutButton = UIButton(type: .detailDisclosure)
            annotationView?.rightCalloutAccessoryView = calloutButton
        } else {
            annotationView?.annotation = annotation
        }
        if(annotationView?.annotation is MKUserLocation){
            // use default annotation
            return nil;
        }else{
            let customPointAnnotation = annotation as! CustomPointAnnotation
            annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)
            return annotationView
        }
    }
    
    @IBAction func fuelPumpLightBox(_ sender: UIButton) {
        if (bill?.fuelPumpImage != ""){
            let images = [
                LightboxImage(imageURL: URL(string:(bill?.fuelPumpImage)!)!),
            ]
            
            let controller = LightboxController(images: images)
            controller.dynamicBackground = true
            
            present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func vehiclePlateLightBox(_ sender: UIButton) {
        if(bill?.vehiclePlateImage != ""){
            let images = [
                LightboxImage(imageURL: URL(string:(bill?.vehiclePlateImage)!)!),
                ]
            
            let controller = LightboxController(images: images)
            controller.dynamicBackground = true
            
            present(controller, animated: true, completion: nil)
        }
    }
}
