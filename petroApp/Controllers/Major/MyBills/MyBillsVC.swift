//
//  MyBillsVC.swift
//  petroApp
//
//  Created by Elsman on 8/13/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class MyBillsVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    var billsItems:[BillData]?
    var selectedBill: BillData?
    
    @IBOutlet var myBills: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        LeftNavBarItems(MenuBtn())
        title = "My Bills".locale
        loadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  billsItems != nil ? (billsItems?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myBillsCellID", for: indexPath) as!MyBillsCell
        cell.selectionStyle = .none
        cell.config(billsItems![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        let viewController = UIStoryboard(name: "Bills", bundle: nil).instantiateViewController(withIdentifier: "MyBillsDetailsVC") as! MyBillsDetailsVC
        viewController.bill = billsItems?[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK:- load cars Data
    func loadData(){
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "\("getBills".route)?lang=\(lang)", method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        let bills = response.data!["data"].map{BillData($0.1)}
                        self.billsItems = bills
                        self.myBills.reloadData()
                    }
                }
                else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
    
}
