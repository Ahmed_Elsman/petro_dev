//
//  ReportsDetailsVC.swift
//  petroApp
//
//  Created by Elsman on 8/14/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class ReportsDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var reportsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Reports Details".locale

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myBillsCellID", for: indexPath) as!MyBillsCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        print("You tapped cell number \(indexPath.row).")
        //        selectedVehicle = vehicleItems?[indexPath.row]
        performSegue(withIdentifier: Config.Present_My_Bills_Details, sender: self)
    }
}
