//
//  ReportsVC.swift
//  petroApp
//
//  Created by Elsman on 8/14/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class ReportsVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var flipView: UIView!
    var userType : String = ""
    var reportItems:[Report]?
    var oneReport: Report?
    var oneVehicle: NSDictionary?
    var fuelTypeID : String = ""
    var dateType : String = ""
    var paymentMethod : String = ""
    var paymentStatus : String = ""
    let datePicker = UIDatePicker()
    var selectedVehicleIndexes : NSMutableArray = NSMutableArray()
    @IBOutlet var reportsTable: UITableView!
    @IBOutlet var popUpView: UIView!
    @IBOutlet weak var bottomHeaderSearch: UIView!
    @IBOutlet weak var noResultView: UIView!
    @IBOutlet weak var consumedLiters: UILabel!
    @IBOutlet weak var consumedDate: UILabel!
    @IBOutlet weak var spentMoney: UILabel!
    @IBOutlet weak var spentMoneyDate: UILabel!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var invoiceNumber: UITextField!
    @IBOutlet weak var amountTo: UITextField!
    @IBOutlet weak var amountFrom: UITextField!
    @IBOutlet weak var dateFrom: UITextField!
    @IBOutlet weak var dateTo: UITextField!
    @IBOutlet weak var button91: UIButton!
    @IBOutlet weak var button95: UIButton!
    @IBOutlet weak var buttonDiesel: UIButton!
    @IBOutlet weak var buttonAll: UIButton!
    @IBOutlet weak var paymentMadeSwitch: UISwitch!
    @IBOutlet weak var paymentNotMadeSwitch: UISwitch!
    @IBOutlet weak var cashSwitch: UISwitch!
    @IBOutlet weak var balanceSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Reports".locale
        if(selectedVehicleIndexes.count == 0){
            userType == "User" ? LeftNavBarItems(MenuBtn()) : LeftNavBarItems(VehicleMenuBtn())
        }else if (userType == "Delegate"){
            LeftNavBarItems(VehicleMenuBtn())
        }
        self.popUpView.isHidden = true
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setup()
    }
    
    func setup(){
        button91.setIcon(icon: .ionicons(.iosCircleOutline), iconSize: 27, color: .white, forState: .normal)
        button91.setIcon(icon: .ionicons(.iosCircleFilled), iconSize: 27, color: .white, forState: .selected)
        
        button95.setIcon(icon: .ionicons(.iosCircleOutline), iconSize: 27, color: .white, forState: .normal)
        button95.setIcon(icon: .ionicons(.iosCircleFilled), iconSize: 27, color: .white, forState: .selected)
        
        buttonDiesel.setIcon(icon: .ionicons(.iosCircleOutline), iconSize: 27, color: .white, forState: .normal)
        buttonDiesel.setIcon(icon: .ionicons(.iosCircleFilled), iconSize: 27, color: .white, forState: .selected)
        
        buttonAll.setIcon(icon: .ionicons(.iosCircleOutline), iconSize: 27, color: .white, forState: .normal)
        buttonAll.setIcon(icon: .ionicons(.iosCircleFilled), iconSize: 27, color: .white, forState: .selected)
        
        bottomHeaderSearch.roundCorners([.topLeft, .topRight], radius: 5)
        filterView.roundCorners([.topLeft, .topRight], radius: 5)
        dateFrom.setLeftPaddingPoints(30)
        dateTo.setLeftPaddingPoints(30)
        
        amountFrom.setRightPaddingPoints(35)
        amountTo.setRightPaddingPoints(35)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dateTo{
            dateType = "To".locale
            showDatePicker()
        }else if textField == dateFrom{
            dateType = "From".locale
            showDatePicker()
        }
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".locale, style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".locale, style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        if(dateType == "To".locale){
            dateTo.inputAccessoryView = toolbar
            dateTo.inputView = datePicker
        }else{
            dateFrom.inputAccessoryView = toolbar
            dateFrom.inputView = datePicker
        }
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        if(dateType == "To".locale){
            dateTo.text = formatter.string(from: datePicker.date)
        }else{
            dateFrom.text = formatter.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @IBAction func openPopUp(_ sender: Any)
    {
        setView(view: popUpView, hidden: false)
        setView(view: bottomHeaderSearch, hidden: true)
    }
    
    @IBAction func hidePopUp(_ sender: Any)
    {
        setView(view: popUpView, hidden: true)
        setView(view: bottomHeaderSearch, hidden: false)
    }
    
    @IBAction func searchBtnTouched(_ sender: Any)
    {
        self.view.endEditing(true)
        loadData()
        setView(view: popUpView, hidden: true)
        setView(view: bottomHeaderSearch, hidden: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (userType == "Delegate" && oneVehicle != nil){
            return 1
        }else{
            if(reportItems != nil){
                ((reportItems?.count)! > 0) ? (reportsTable.isHidden = false) : (reportsTable.isHidden = true)
                ((reportItems?.count)! > 0) ? (noResultView.isHidden = true) : (noResultView.isHidden = false)
            }
            return  reportItems != nil ? (reportItems?.count)! : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reportCellID", for: indexPath) as!ReportCell
        cell.selectionStyle = .none
        if (userType == "Delegate"){
            cell.configVehicle(oneVehicle!,numberOfLittres: self.consumedLiters.text! as NSString ,costValue: self.spentMoney.text! as NSString)
        }else{
            cell.config(reportItems![indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
//        let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
//        viewController.vehicle = reportItems![indexPath.row].vehicle
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    @IBAction func button91BtnTouhed(_ sender: UIButton) {
        fuelTypeID = "1"
        button91.isSelected = true
        button95.isSelected = false
        buttonDiesel.isSelected = false
        buttonAll.isSelected = false
    }
    
    @IBAction func button95BtnTouhed(_ sender: UIButton) {
        fuelTypeID = "2"
        button91.isSelected = false
        button95.isSelected = true
        buttonDiesel.isSelected = false
        buttonAll.isSelected = false
    }
    
    @IBAction func dieselBtnTouhed(_ sender: UIButton) {
        fuelTypeID = "3"
        button91.isSelected = false
        button95.isSelected = false
        buttonDiesel.isSelected = true
        buttonAll.isSelected = false
    }
    
    @IBAction func allBtnTouhed(_ sender: UIButton) {
        fuelTypeID = ""
        button91.isSelected = false
        button95.isSelected = false
        buttonDiesel.isSelected = false
        buttonAll.isSelected = true
    }
    @IBAction func paidSwitchChanged(_ sender: UISwitch) {
        paymentMadeSwitch.isOn && paymentNotMadeSwitch.isOn ? (paymentMethod = "") : paymentMadeSwitch.isOn && !paymentNotMadeSwitch.isOn ? (paymentMethod = "1") : (paymentMethod = "2")
    }
    @IBAction func notPaidSwitchChanged(_ sender: UISwitch) {
        paymentMadeSwitch.isOn && paymentNotMadeSwitch.isOn ? (paymentMethod = "") : paymentMadeSwitch.isOn && !paymentNotMadeSwitch.isOn ? (paymentMethod = "1") : (paymentMethod = "2")
    }
    @IBAction func cashSwitchChanged(_ sender: UISwitch) {
        cashSwitch.isOn && balanceSwitch.isOn ? (paymentStatus = "") : cashSwitch.isOn && !balanceSwitch.isOn ? (paymentStatus = "1") : (paymentStatus = "2")
    }
    @IBAction func balanceSwitchChanged(_ sender: Any) {
        cashSwitch.isOn && balanceSwitch.isOn ? (paymentStatus = "") : cashSwitch.isOn && !balanceSwitch.isOn ? (paymentStatus = "1") : (paymentStatus = "2")
    }
    // MARK:- load cars Data
    func loadData(){
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        var param:[String: Any] = ["lang":lang]
        if(selectedVehicleIndexes.count > 0){
            bottomHeaderSearch.isHidden = true
            var vehicleIDs : String = ""
            for (index,selectedVehicleID) in selectedVehicleIndexes.enumerated(){
                (vehicleIDs == "") && (index < selectedVehicleIndexes.count) ? (vehicleIDs = "\(selectedVehicleID)") : (index < selectedVehicleIndexes.count) ? (vehicleIDs = "\(vehicleIDs),\(selectedVehicleID)") : (vehicleIDs = "\(vehicleIDs),\(selectedVehicleID)")
            }
            param["vehicles_ids"] = "[\(vehicleIDs)]"
        }else{
            param["from_date"] = self.dateFrom.text ?? ""
            param["to_date"] = self.dateTo.text ?? ""
            param["payment_method"] = paymentMethod
            param["fueltype"] = fuelTypeID
            if(self.amountFrom.text == "" && self.amountTo.text == ""){
                param["cost"] = ""
            }else{
                param["cost"] = "\(self.amountFrom.text ?? ""),\(self.amountTo.text ?? "")"
            }
            param["bill_number"] = self.invoiceNumber.text ?? ""
            param["payment_status"] = paymentStatus
        }
        var fullLink : String = ""
        if (userType == "Delegate"){
            fullLink = "\("getVehicleReport".route)/\(UserUtil.loadUser()?.vehicle.id ?? 0)"
            Requests.instance.request(link: fullLink, method: .get) { (response) in
                print(response)
                self.StopLoading()
                if response.data!["success"].bool == false{
                    self.reportsTable.isHidden = true
                    self.noResultView.isHidden = false
                }else{
                    if response.data!["success"].bool == true {
                        print(response)
                        self.view.endEditing(true)
                        DispatchQueue.main.async {
                            let vehicle = response.data!["data"]["vehcile"].rawValue
                            self.oneVehicle = vehicle as? NSDictionary
                            self.consumedLiters.text = "\((response.data!["data"]["num_of_liters"]).doubleValue)"
                            self.spentMoney.text = "\((response.data!["data"]["total_cost"]).doubleValue)"
                            self.reportsTable.reloadData()
                        }
                    }
                    else{
                        self.view.endEditing(true)
                        self.showErrorMessage(response.data!)
                    }
                }
            }
        }else{
            fullLink = "getVehiclesReports".route
            Requests.instance.request(link: fullLink, method: .post, parameters: param) { (response) in
                print(response)
                self.StopLoading()
                if response.data!["success"].bool == false{
                    self.reportsTable.isHidden = true
                    self.noResultView.isHidden = false
                }else{
                    if response.data!["success"].bool == true {
                        print(response)
                        self.view.endEditing(true)
                        DispatchQueue.main.async {
                            let reports = response.data!["data"]["reports"].map{Report($0.1)}
                            self.consumedLiters.text = "\((response.data!["data"]["totalliters"]).doubleValue)"
                            self.spentMoney.text = "\((response.data!["data"]["totalcost"]).doubleValue)"
                            self.reportItems = reports
                            self.reportsTable.reloadData()
                        }
                    }
                    else{
                        self.view.endEditing(true)
                        self.showErrorMessage(response.data!)
                    }
                }
            }
        }
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}
