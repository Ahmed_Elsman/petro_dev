//
//  VehicleBillsVC.swift
//  petroApp
//
//  Created by Elsman on 9/19/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class VehicleBillsVC: UIViewController , UITableViewDelegate, UITableViewDataSource  {

    var billsItems:[BillData]?
    @IBOutlet var vehicleMyBills: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        LeftNavBarItems(VehicleMenuBtn())
        title = "My Bills".locale
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  billsItems != nil ? (billsItems?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "vehicleBillsCellID", for: indexPath) as!VehicleBillsCell
        cell.selectionStyle = .none
        cell.configDelegate(billsItems![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "Bills", bundle: nil).instantiateViewController(withIdentifier: "MyBillsDetailsVC") as! MyBillsDetailsVC
        viewController.bill = billsItems?[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    // MARK:- load cars Data
    func loadData(){
        StartLoading()
        print( "user token" , UserDefaults.standard.string(forKey: "token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "\("getDelegateBills".route)?lang=\(lang)", method: .get, parameters: param) { (response) in
            print(response)
            self.StopLoading()
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    self.view.endEditing(true)
                    DispatchQueue.main.async {
                        let bills = response.data!["data"].map{BillData($0.1)}
                        self.billsItems = bills
                        self.vehicleMyBills.reloadData()
                    }
                }
                else{
                    self.view.endEditing(true)
                    self.showErrorMessage(response.data!)
                }
            }
        }
    }
}
