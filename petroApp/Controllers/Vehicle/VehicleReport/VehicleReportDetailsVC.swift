//
//  VehicleReportDetailsVC.swift
//  petroApp
//
//  Created by Elsman on 9/19/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class VehicleReportDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var reportDetails: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Reports Details".locale

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "vehicleBillsCellID", for: indexPath) as!VehicleBillsCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        print("You tapped cell number \(indexPath.row).")
        //        selectedVehicle = vehicleItems?[indexPath.row]
        performSegue(withIdentifier: Config.Present_My_Bills_Details, sender: self)
    }

}
