//
//  MyBillsDetailsVC.swift
//  petroApp
//
//  Created by Elsman on 8/13/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import MapKit

class VehicleAccountVC: UIViewController,MKMapViewDelegate {
    var vehicle: SingleCar?
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var fuelpumpImage: UIImageView!
    @IBOutlet weak var billScrollView: UIScrollView!
    @IBOutlet weak var vehiclePlateImage: UIImageView!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleModelName: UILabel!
    @IBOutlet weak var arabicNumbersLabel: UILabel!
    @IBOutlet weak var arabicLettersLabel: UILabel!
    @IBOutlet weak var englishNumbersLabel: UILabel!
    @IBOutlet weak var englishLettersLabel: UILabel!
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var Model: UILabel!
    @IBOutlet weak var Year: UILabel!
    @IBOutlet weak var Balance: UILabel!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var commender: UILabel!
    @IBOutlet weak var fuelType: UILabel!
    @IBOutlet weak var fuelColor: UIView!
    @IBOutlet var carImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "car".locale
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup(){
        vehicle = UserUtil.loadUser()?.vehicle
        vehicleImage.af_setImage(withURL: URL(string:(vehicle?.vehicleImage)!)!)
        vehicleType.text = vehicle?.carType
        fuelType.text = vehicle?.fuelType
        brand.text = vehicle?.carBrand
        Model.text = vehicle?.carModel
        Year.text = "\(vehicle?.carYear ?? 0)"
        self.fuelColor.backgroundColor = UIColor(hex:(vehicle?.fuelTypeColor)!)
        commender.text =  UserUtil.loadUser()?.name
        code.text =  UserUtil.loadUser()?.code
        vehicleName.text = vehicle?.carBrand
        vehicleModelName.text = vehicle?.carModel
        carImage.af_setImage(withURL: URL(string:(vehicle?.plateImage)!)!)
        arabicLettersLabel.text = vehicle?.carPlateLettersAr
        englishLettersLabel.text = String((vehicle?.carPlateLettersEn as! String).reversed())
        arabicNumbersLabel.text = vehicle?.carPlateNumbersAr
        englishNumbersLabel.text = vehicle?.carPlateNumbersEn
        Balance.text = "\(vehicle?.balance ?? 0)"
    }

}
