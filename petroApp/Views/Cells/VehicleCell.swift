//
//  VehicleCell.swift
//  petroApp
//
//  Created by Elsman on 7/23/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import SwipeCellKit

class VehicleCell: SwipeTableViewCell
{

    @IBOutlet weak var commenderImage: UIImageView!
    @IBOutlet weak var commenderName: UILabel!
    @IBOutlet weak var commenderID: UILabel!
    @IBOutlet weak var refreshBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(_ car:Commender) {
        commenderImage.af_setImage(withURL: URL(string:car.image)!)
        commenderName.text = car.name
        commenderID.text = String(car.code)
    }
}
