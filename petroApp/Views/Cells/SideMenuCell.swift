//
//  SideMenuCell.swift
//  petroApp
//
//  Created by Elsman on 7/16/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet var titleText: UILabel!
    @IBOutlet var titleImage: UIImageView!
    @IBOutlet weak var langView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
