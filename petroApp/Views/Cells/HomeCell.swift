//
//  HomeCell.swift
//  petroApp
//
//  Created by Elsman on 7/10/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var arabicNumbersLabel: UILabel!
    @IBOutlet weak var arabicLettersLabel: UILabel!
    @IBOutlet weak var englishNumbersLabel: UILabel!
    @IBOutlet weak var englishLettersLabel: UILabel!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleModelName: UILabel!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet var carImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(_ car:Vehicle) {
        arabicLettersLabel.text = car.carPlateLettersAr
        englishLettersLabel.text = String(car.carPlateLettersEn.reversed())
        arabicNumbersLabel.text = car.carPlateNumbersAr
        englishNumbersLabel.text = car.carPlateNumbersEn
        vehicleName.text = car.carBrand
        vehicleModelName.text = car.carModel
        vehicleImage.af_setImage(withURL: URL(string:car.vehicleImage)!)
        carImage.af_setImage(withURL: URL(string:(car.plateImage)!)!)

    }
    
}
