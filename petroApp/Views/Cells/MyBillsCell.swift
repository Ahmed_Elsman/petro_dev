//
//  MyBillsCell.swift
//  petroApp
//
//  Created by Elsman on 8/13/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class MyBillsCell: UITableViewCell {

    @IBOutlet weak var numberOfLittres: UILabel!
    @IBOutlet weak var billNumber: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var arabicNumbersLabel: UILabel!
    @IBOutlet weak var arabicLettersLabel: UILabel!
    @IBOutlet weak var englishNumbersLabel: UILabel!
    @IBOutlet weak var englishLettersLabel: UILabel!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleModelName: UILabel!
    @IBOutlet weak var vehicleNumberImage: UIImageView!
    @IBOutlet weak var billDate: UILabel!
    @IBOutlet weak var billTime: UILabel!
    @IBOutlet var carImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(_ bill:BillData) {
        arabicLettersLabel.text = bill.delegate.vehicle.carPlateLettersAr
        englishLettersLabel.text = String(bill.delegate.vehicle.carPlateLettersEn.reversed())
        arabicNumbersLabel.text = bill.delegate.vehicle.carPlateNumbersAr
        englishNumbersLabel.text = bill.delegate.vehicle.carPlateNumbersEn
        vehicleImage.af_setImage(withURL: URL(string:bill.delegate.vehicle.vehicleImage)!)
        vehicleName.text = bill.delegate.vehicle.carBrand
        vehicleModelName.text = bill.delegate.vehicle.carModel
        billNumber.text = String(bill.id)
        numberOfLittres.text = String(bill.litters)
        cost.text = "\(String(bill.cost)) SAR"
        let created = bill.created
        billDate.text = created?.substring(9)
        billTime.text = String((created?.prefix(8))!)
        carImage.af_setImage(withURL: URL(string:(bill.delegate.vehicle.plateImage)!)!)
    }
}
