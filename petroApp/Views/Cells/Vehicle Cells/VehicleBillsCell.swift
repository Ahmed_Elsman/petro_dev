//
//  VehicleBillsCell.swift
//  petroApp
//
//  Created by Elsman on 9/19/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class VehicleBillsCell: UITableViewCell {

    @IBOutlet weak var numberOfLittres: UILabel!
    @IBOutlet weak var paymentMethod: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var operationNumber: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var billDate: UILabel!
    @IBOutlet weak var billTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configDelegate(_ bill:BillData) {
        operationNumber.text = String(bill.id)
        paymentMethod.text = String(bill.paymentMethod)
        status.text = String(bill.statusName)
        numberOfLittres.text = String(bill.litters)
        cost.text = "\(String(bill.cost)) SAR"
        let created = bill.created
        billDate.text = created?.substring(9)
        billTime.text = String((created?.prefix(8))!)
    }
}
