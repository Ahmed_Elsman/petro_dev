//
//  HomeCell.swift
//  petroApp
//
//  Created by Elsman on 7/10/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {

    @IBOutlet weak var arabicNumbersLabel: UILabel!
    @IBOutlet weak var arabicLettersLabel: UILabel!
    @IBOutlet weak var englishNumbersLabel: UILabel!
    @IBOutlet weak var englishLettersLabel: UILabel!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleModelName: UILabel!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var numberOfLettres: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet var carImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configVehicle(_ vehicle:NSDictionary, numberOfLittres:NSString ,costValue:NSString ) {
        numberOfLettres.text = "\(numberOfLittres) litre"
        cost.text = "\(costValue) SAR"
        arabicLettersLabel.text = vehicle["car_plate_letters_ar"] as? String
        englishLettersLabel.text = vehicle["car_plate_letters_en"] as? String
        arabicNumbersLabel.text = vehicle["car_plate_numbers_ar"] as? String
        englishNumbersLabel.text = vehicle["car_plate_numbers_en"] as? String
        vehicleName.text = vehicle["car_brand"] as? String
        vehicleModelName.text = vehicle["car_model"] as? String
        vehicleImage.af_setImage(withURL: URL(string:(vehicle["vehicle_image"])! as! String)!)
        carImage.af_setImage(withURL: URL(string:(vehicle["plate_image"])! as! String)!)
    }
    func config(_ report:Report) {
        numberOfLettres.text = "\(String(report.numOfLiters)) litre"
        cost.text = "\(String(report.totalCost)) SAR"
        arabicLettersLabel.text = report.vehicle.carPlateLettersAr
        englishLettersLabel.text = String(report.vehicle.carPlateLettersEn.reversed())
        arabicNumbersLabel.text = report.vehicle.carPlateNumbersAr
        englishNumbersLabel.text = report.vehicle.carPlateNumbersEn
        vehicleName.text = report.vehicle.carBrand
        vehicleModelName.text = report.vehicle.carModel
        vehicleImage.af_setImage(withURL: URL(string:report.vehicle.vehicleImage)!)
        carImage.af_setImage(withURL: URL(string:(report.vehicle.plateImage)!)!)
    }
    
}
