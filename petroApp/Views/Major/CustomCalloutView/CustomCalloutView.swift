//
//  CustomCalloutView.swift
//  CustomCalloutView
//
//  Created by Malek T. on 3/10/16.
//  Copyright © 2016 Medigarage Studios LTD. All rights reserved.
//

import UIKit

class CustomCalloutView: UIView {

    @IBOutlet var stationName: UILabel!
    @IBOutlet var stationAddress: UILabel!
    @IBOutlet weak var detailsBtn: UIButton!
    @IBOutlet weak var detailsArrow: UIImageView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
