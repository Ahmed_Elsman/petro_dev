//
//  StoreFollower.swift
//  TwitterApp
//
//  Created by Mina Gad on 2/7/18.
//  Copyright © 2018 Mina Gad. All rights reserved.
//

import Foundation

// through this class i handle all persistance operation of whole project like saving deleting, appending, .....
final class PeopleStore {
    
    // singleton object......... that manage all operation of database  in system......
    static let shared = PeopleStore()
    
    private init()
    {
        // get all people from document dirctory in application Sandbox
        if let archivedPeoples = NSKeyedUnarchiver.unarchiveObject(withFile: peopleArchiveURL.path) as? [User] {
            savedPeople = archivedPeoples
        }
    }
    /////
    
    // refrence old people
    var savedPeople = [User]()
    
    // refrence new people if connected to internet
    var newPeople = [User]()
    
    // path on mobile sand box
    let peopleArchiveURL: URL = {
        let documentsDirectories =
            FileManager.default.urls(for: .documentDirectory,
                                     in: .userDomainMask)
        let documentDirectory = documentsDirectories.first!
        return documentDirectory.appendingPathComponent("peoples.archive")
    }()
    
    // 3 helper Functions......
    func append(with peoples: [User], page: Int) {
        if page == 1 {
            self.newPeople = peoples
        }
        else {
            self.newPeople += peoples
        }
    }

    
    func loadUser(withId id:Int) -> User? {
        let allUsers = self.savedPeople
        return allUsers.first(where: {$0.id == id})
    }
    
    func updateUser(forUser user:User) {
        var allUsers = self.savedPeople
        if let index = allUsers.index(where: {$0.id == user.id}) {
            allUsers[index] = user
            self.newPeople = allUsers
            let _ = updateDatabase()
        }
        
    }
    
    
    /////////////////////
    // save to disk.......
    func save() -> Bool {
        //        print("followers path url \(followersArchiveURL)")
        return NSKeyedArchiver.archiveRootObject(newPeople, toFile: peopleArchiveURL.path)
    }
    
    func updateDatabase() -> Bool {
        do{
            // remove old followers from mobile memory and update database...... with new followers
            let exist = FileManager.default.fileExists(atPath: peopleArchiveURL.path)
            let connected = helper.connected
            if exist ,connected {
                try FileManager.default.removeItem(atPath: peopleArchiveURL.path)
            }
            if !newPeople.isEmpty {
                let x = save()
                x ? print("saved updates successfully") : print("error in updating database \(peopleArchiveURL.path)")
                return true
            }
        }
        catch {
            print("error in updating database \(error.localizedDescription)")
        }
        return false
    }
    
}

