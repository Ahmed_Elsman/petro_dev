//
//  UserStore.swift
//  Events
//
//  Created by Elsman on 1/31/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserUtil {
    
    private static let UserKey = "User"
    private static let Token = "token"
    private static let Password = "password"
    private static let AccountType = "type"
    private static func archiveUser(_ user :User) -> NSData {
        return NSKeyedArchiver.archivedData(withRootObject: user as User) as NSData
    }
    static func loadUser() -> User? {
        if let unarchivedObject = UserDefaults.standard.object(forKey: UserKey) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? User
        }
        return nil
    }
    
    static func saveUser(_ user : User?) {
        let archivedObject = archiveUser(user!)
        UserDefaults.standard.set(archivedObject, forKey: UserKey)
        UserDefaults.standard.synchronize()
    }
    
    static func removeUser() {
        UserDefaults.standard.removeObject(forKey: UserKey)
        UserDefaults.standard.synchronize()
    }
    
    static func saveUserToken(_ token :JSON) {
        UserDefaults.standard.set(token["refresh_token"].description, forKey: Token)
        UserDefaults.standard.synchronize()
    }
    
    static func saveUserPassword(_ password :String) {
        UserDefaults.standard.set(password, forKey: Password)
        UserDefaults.standard.synchronize()
    }
    
    static func saveUserType(_ type :String) {
        UserDefaults.standard.set(type, forKey: AccountType)
        UserDefaults.standard.synchronize()
    }
    
    static func loadUserToken() -> String? {
        if let unarchivedObject = UserDefaults.standard.object(forKey: Token) as? String {
            return unarchivedObject
        }
        return nil
    }
    
    static func loadUserPassword() -> String? {
        if let unarchivedObject = UserDefaults.standard.object(forKey: Password) as? String {
            return unarchivedObject
        }
        return nil
    }
    
    static func loadUserType() -> String? {
        if let unarchivedObject = UserDefaults.standard.object(forKey: AccountType) as? String {
            return unarchivedObject
        }
        return nil
    }
    
    static func removeUserToken() {
        UserDefaults.standard.removeObject(forKey: Token)
        UserDefaults.standard.synchronize()
    }
    
    static func removeUserPassword() {
        UserDefaults.standard.removeObject(forKey: Password)
        UserDefaults.standard.synchronize()
    }
}
