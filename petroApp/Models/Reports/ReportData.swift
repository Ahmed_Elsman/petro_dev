//
//	ReportData.swift
//
//	Create by ahmed elsman on 18/9/2018
//	Copyright © 2018. All rights reserved.


import Foundation 
import SwiftyJSON


class ReportData : NSObject, NSCoding{

	var reports : [Report]!
	var totalcost : Float!
	var totalliters : Float!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required  init(_ json: JSON!){
		if json.isEmpty{
			return
		}
		reports = [Report]()
		let reportsArray = json["reports"].arrayValue
		for reportsJson in reportsArray{
			let value = Report(_ : reportsJson)
			reports.append(value)
		}
		totalcost = json["totalcost"].floatValue
		totalliters = json["totalliters"].floatValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if reports != nil{
			var dictionaryElements = [[String:Any]]()
			for reportsElement in reports {
				dictionaryElements.append(reportsElement.toDictionary())
			}
			dictionary["reports"] = dictionaryElements
		}
		if totalcost != nil{
			dictionary["totalcost"] = totalcost
		}
		if totalliters != nil{
			dictionary["totalliters"] = totalliters
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         reports = aDecoder.decodeObject(forKey: "reports") as? [Report]
         totalcost = aDecoder.decodeObject(forKey: "totalcost") as? Float
         totalliters = aDecoder.decodeObject(forKey: "totalliters") as? Float

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if reports != nil{
			aCoder.encode(reports, forKey: "reports")
		}
		if totalcost != nil{
			aCoder.encode(totalcost, forKey: "totalcost")
		}
		if totalliters != nil{
			aCoder.encode(totalliters, forKey: "totalliters")
		}

	}

}
