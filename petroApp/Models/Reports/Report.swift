//
//	Report.swift
//
//	Create by ahmed elsman on 18/9/2018
//	Copyright © 2018. All rights reserved.


import Foundation 
import SwiftyJSON


class Report : NSObject, NSCoding{

	var numOfLiters : Float!
	var totalCost : Int!
	var vehicle : Vehicle!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required  init(_ json: JSON!){
		if json.isEmpty{
			return
		}
		numOfLiters = json["num_of_liters"].floatValue
		totalCost = json["total_cost"].intValue
		let vehicleJson = json["vehicle"]
		if !vehicleJson.isEmpty{
			vehicle = Vehicle(_ : vehicleJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if numOfLiters != nil{
			dictionary["num_of_liters"] = numOfLiters
		}
		if totalCost != nil{
			dictionary["total_cost"] = totalCost
		}
		if vehicle != nil{
			dictionary["vehicle"] = vehicle.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         numOfLiters = aDecoder.decodeObject(forKey: "num_of_liters") as? Float
         totalCost = aDecoder.decodeObject(forKey: "total_cost") as? Int
         vehicle = aDecoder.decodeObject(forKey: "vehicle") as? Vehicle

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if numOfLiters != nil{
			aCoder.encode(numOfLiters, forKey: "num_of_liters")
		}
		if totalCost != nil{
			aCoder.encode(totalCost, forKey: "total_cost")
		}
		if vehicle != nil{
			aCoder.encode(vehicle, forKey: "vehicle")
		}

	}

}
