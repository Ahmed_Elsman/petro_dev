//
//	Data.swift
//
//	Create by ahmed elsman on 18/9/2018
//	Copyright © 2018. All rights reserved.

import Foundation 
import SwiftyJSON


class BillData : NSObject{

	var branch : String!
	var branchAddress : String!
	var cost : Int!
	var created : String!
	var delegate : Delegate!
	var fuelPumpImage : String!
	var id : Int!
	var lat : Float!
	var litters : Float!
	var lng : Float!
	var paymentMethod : String!
	var status : Int!
    var statusName : String!
	var vehiclePlateImage : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required  init(_ json: JSON!){
		if json.isEmpty{
			return
		}
		branch = json["branch"].stringValue
		branchAddress = json["branch_address"].stringValue
		cost = json["cost"].intValue
		created = json["created"].stringValue
		let delegateJson = json["delegate"]
		if !delegateJson.isEmpty{
			delegate = Delegate(_ : delegateJson)
		}
		fuelPumpImage = json["fuel_pump_image"].stringValue
		id = json["id"].intValue
		lat = json["lat"].floatValue
		litters = json["litters"].floatValue
		lng = json["lng"].floatValue
		paymentMethod = json["payment_method"].stringValue
		status = json["status"].intValue
        statusName = json["status_name"].stringValue
		vehiclePlateImage = json["vehicle_plate_image"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if branch != nil{
			dictionary["branch"] = branch
		}
		if branchAddress != nil{
			dictionary["branch_address"] = branchAddress
		}
		if cost != nil{
			dictionary["cost"] = cost
		}
		if created != nil{
			dictionary["created"] = created
		}
		if delegate != nil{
			dictionary["delegate"] = delegate.toDictionary()
		}
		if fuelPumpImage != nil{
			dictionary["fuel_pump_image"] = fuelPumpImage
		}
		if id != nil{
			dictionary["id"] = id
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if litters != nil{
			dictionary["litters"] = litters
		}
		if lng != nil{
			dictionary["lng"] = lng
		}
		if paymentMethod != nil{
			dictionary["payment_method"] = paymentMethod
		}
        if status != nil{
            dictionary["status"] = status
        }
        if statusName != nil{
            dictionary["status_name"] = statusName
        }
        
		if vehiclePlateImage != nil{
			dictionary["vehicle_plate_image"] = vehiclePlateImage
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         branch = aDecoder.decodeObject(forKey: "branch") as? String
         branchAddress = aDecoder.decodeObject(forKey: "branch_address") as? String
         cost = aDecoder.decodeObject(forKey: "cost") as? Int
         created = aDecoder.decodeObject(forKey: "created") as? String
         delegate = aDecoder.decodeObject(forKey: "delegate") as? Delegate
         fuelPumpImage = aDecoder.decodeObject(forKey: "fuel_pump_image") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         lat = aDecoder.decodeObject(forKey: "lat") as? Float
         litters = aDecoder.decodeObject(forKey: "litters") as? Float
         lng = aDecoder.decodeObject(forKey: "lng") as? Float
         paymentMethod = aDecoder.decodeObject(forKey: "payment_method") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int
         statusName = aDecoder.decodeObject(forKey: "status_name") as? String
         vehiclePlateImage = aDecoder.decodeObject(forKey: "vehicle_plate_image") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if branch != nil{
			aCoder.encode(branch, forKey: "branch")
		}
		if branchAddress != nil{
			aCoder.encode(branchAddress, forKey: "branch_address")
		}
		if cost != nil{
			aCoder.encode(cost, forKey: "cost")
		}
		if created != nil{
			aCoder.encode(created, forKey: "created")
		}
		if delegate != nil{
			aCoder.encode(delegate, forKey: "delegate")
		}
		if fuelPumpImage != nil{
			aCoder.encode(fuelPumpImage, forKey: "fuel_pump_image")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if litters != nil{
			aCoder.encode(litters, forKey: "litters")
		}
		if lng != nil{
			aCoder.encode(lng, forKey: "lng")
		}
		if paymentMethod != nil{
			aCoder.encode(paymentMethod, forKey: "payment_method")
		}
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if statusName != nil{
            aCoder.encode(statusName, forKey: "status_name")
        }
		if vehiclePlateImage != nil{
			aCoder.encode(vehiclePlateImage, forKey: "vehicle_plate_image")
		}

	}

}
