//
//	Delegate.swift
//
//	Create by ahmed elsman on 18/9/2018
//	Copyright © 2018. All rights reserved.

import Foundation 
import SwiftyJSON


class Delegate : NSObject{

	var code : String!
	var image : String!
	var name : String!
	var vehicle : SingleCar!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required  init(_ json: JSON!){
		if json.isEmpty{
			return
		}
		code = json["code"].stringValue
		image = json["image"].stringValue
		name = json["name"].stringValue
		let vehicleJson = json["vehicle"]
		if !vehicleJson.isEmpty{
			vehicle = SingleCar(_ : vehicleJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if code != nil{
			dictionary["code"] = code
		}
		if image != nil{
			dictionary["image"] = image
		}
		if name != nil{
			dictionary["name"] = name
		}
		if vehicle != nil{
			dictionary["vehicle"] = vehicle.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         code = aDecoder.decodeObject(forKey: "code") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         vehicle = aDecoder.decodeObject(forKey: "vehicle") as? SingleCar

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if vehicle != nil{
			aCoder.encode(vehicle, forKey: "vehicle")
		}

	}

}
