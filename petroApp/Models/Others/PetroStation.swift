//
//	PetroStation.swift
//
//	Create by ahmed elsman on 26/9/2018
//	Copyright © 2018. All rights reserved.

import Foundation 
import SwiftyJSON


class PetroStation : NSObject, NSCoding{

	var address : String!
	var distance : Float!
	var hasServices : Bool!
	var id : Int!
	var isDiesel : Bool!
	var lat : Double!
	var lng : Double!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required  init(_ json: JSON!){
		if json.isEmpty{
			return
		}
		address = json["address"].stringValue
		distance = json["distance"].floatValue
		hasServices = json["has_services"].boolValue
		id = json["id"].intValue
		isDiesel = json["isDiesel"].boolValue
		lat = json["lat"].doubleValue
		lng = json["lng"].doubleValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if address != nil{
			dictionary["address"] = address
		}
		if distance != nil{
			dictionary["distance"] = distance
		}
		if hasServices != nil{
			dictionary["has_services"] = hasServices
		}
		if id != nil{
			dictionary["id"] = id
		}
		if isDiesel != nil{
			dictionary["isDiesel"] = isDiesel
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if lng != nil{
			dictionary["lng"] = lng
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         distance = aDecoder.decodeObject(forKey: "distance") as? Float
         hasServices = aDecoder.decodeObject(forKey: "has_services") as? Bool
         id = aDecoder.decodeObject(forKey: "id") as? Int
         isDiesel = aDecoder.decodeObject(forKey: "isDiesel") as? Bool
         lat = aDecoder.decodeObject(forKey: "lat") as? Double
         lng = aDecoder.decodeObject(forKey: "lng") as? Double
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if distance != nil{
			aCoder.encode(distance, forKey: "distance")
		}
		if hasServices != nil{
			aCoder.encode(hasServices, forKey: "has_services")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isDiesel != nil{
			aCoder.encode(isDiesel, forKey: "isDiesel")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lng != nil{
			aCoder.encode(lng, forKey: "lng")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}
