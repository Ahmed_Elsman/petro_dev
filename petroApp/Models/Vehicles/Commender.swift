//
//	Commender.swift
//
//	Create by ahmed elsman on 26/9/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Commender : NSObject, NSCoding{

	var id : Int!
	var image : String!
	var name : String!
    var code : Int!
    var username : String!
    var mobile : String!
    var commenderImage : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required  init(_ json: JSON!){
		if json.isEmpty{
			return
		}
		id = json["id"].intValue
		image = json["image"].stringValue
		name = json["name"].stringValue
        code = json["code"].intValue
        username = json["username"].stringValue
        mobile = json["mobile"].stringValue
        commenderImage = json["image"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
        if name != nil{
            dictionary["name"] = name
        }
        if code != nil{
            dictionary["code"] = code
        }
        if username != nil{
            dictionary["username"] = code
        }
        if mobile != nil{
            dictionary["mobile"] = code
        }
        if commenderImage != nil{
            dictionary["image"] = code
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         code = aDecoder.decodeObject(forKey: "code") as? Int
        username = aDecoder.decodeObject(forKey: "username") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        commenderImage = aDecoder.decodeObject(forKey: "image") as? String
        
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if code != nil{
            aCoder.encode(code, forKey: "code")
        }
        if username != nil{
            aCoder.encode(code, forKey: "username")
        }
        if mobile != nil{
            aCoder.encode(code, forKey: "mobile")
        }
        if commenderImage != nil{
            aCoder.encode(code, forKey: "image")
        }

	}

}
