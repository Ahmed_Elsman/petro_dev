//
//	Vehicle.swift
//
//	Create by ahmed elsman on 12/7/2018
//	Copyright © 2018. All rights reserved.


import Foundation 
import SwiftyJSON


class Vehicle : NSObject{

	var carBrand : String!
	var carModel : String!
    var carType : String!
	var carPlateLettersAr : String!
	var carPlateLettersEn : String!
	var carPlateNumbersAr : String!
	var carPlateNumbersEn : String!
	var id : Int!
	var plateImage : String!
	var vehicleImage : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required  init(_ json: JSON!){
		if json.isEmpty{
			return
		}
		carBrand = json["car_brand"].stringValue
		carModel = json["car_model"].stringValue
        carType = json["car_type"].stringValue
		carPlateLettersAr = json["car_plate_letters_ar"].stringValue
		carPlateLettersEn = json["car_plate_letters_en"].stringValue
		carPlateNumbersAr = json["car_plate_numbers_ar"].stringValue
		carPlateNumbersEn = json["car_plate_numbers_en"].stringValue
		id = json["id"].intValue
		plateImage = json["plate_image"].stringValue
		vehicleImage = json["vehicle_image"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if carBrand != nil{
			dictionary["car_brand"] = carBrand
		}
        if carModel != nil{
            dictionary["car_model"] = carModel
        }
        if carType != nil{
            dictionary["car_type"] = carModel
        }
		if carPlateLettersAr != nil{
			dictionary["car_plate_letters_ar"] = carPlateLettersAr
		}
		if carPlateLettersEn != nil{
			dictionary["car_plate_letters_en"] = carPlateLettersEn
		}
		if carPlateNumbersAr != nil{
			dictionary["car_plate_numbers_ar"] = carPlateNumbersAr
		}
		if carPlateNumbersEn != nil{
			dictionary["car_plate_numbers_en"] = carPlateNumbersEn
		}
		if id != nil{
			dictionary["id"] = id
		}
		if plateImage != nil{
			dictionary["plate_image"] = plateImage
		}
		if vehicleImage != nil{
			dictionary["vehicle_image"] = vehicleImage
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         carBrand = aDecoder.decodeObject(forKey: "car_brand") as? String
         carModel = aDecoder.decodeObject(forKey: "car_model") as? String
        carModel = aDecoder.decodeObject(forKey: "car_type") as? String
         carPlateLettersAr = aDecoder.decodeObject(forKey: "car_plate_letters_ar") as? String
         carPlateLettersEn = aDecoder.decodeObject(forKey: "car_plate_letters_en") as? String
         carPlateNumbersAr = aDecoder.decodeObject(forKey: "car_plate_numbers_ar") as? String
         carPlateNumbersEn = aDecoder.decodeObject(forKey: "car_plate_numbers_en") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         plateImage = aDecoder.decodeObject(forKey: "plate_image") as? String
         vehicleImage = aDecoder.decodeObject(forKey: "vehicle_image") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if carBrand != nil{
			aCoder.encode(carBrand, forKey: "car_brand")
		}
        if carModel != nil{
            aCoder.encode(carModel, forKey: "car_model")
        }
        if carType != nil{
            aCoder.encode(carModel, forKey: "car_type")
        }
		if carPlateLettersAr != nil{
			aCoder.encode(carPlateLettersAr, forKey: "car_plate_letters_ar")
		}
		if carPlateLettersEn != nil{
			aCoder.encode(carPlateLettersEn, forKey: "car_plate_letters_en")
		}
		if carPlateNumbersAr != nil{
			aCoder.encode(carPlateNumbersAr, forKey: "car_plate_numbers_ar")
		}
		if carPlateNumbersEn != nil{
			aCoder.encode(carPlateNumbersEn, forKey: "car_plate_numbers_en")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if plateImage != nil{
			aCoder.encode(plateImage, forKey: "plate_image")
		}
		if vehicleImage != nil{
			aCoder.encode(vehicleImage, forKey: "vehicle_image")
		}

	}

}
