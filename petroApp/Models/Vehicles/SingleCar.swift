//
//	SingleCar.swift
//
//	Create by ahmed elsman on 26/9/2018
//	Copyright © 2018. All rights reserved.

import Foundation 
import SwiftyJSON


class SingleCar : NSObject, NSCoding{

	var balance : Float!
	var carBrand : String!
	var carBrandId : Int!
	var carModel : String!
	var carModelId : Int!
	var carPlateLettersAr : String!
	var carPlateLettersEn : String!
	var carPlateNumbersAr : String!
	var carPlateNumbersEn : String!
	var carType : String!
	var carTypeId : Int!
	var carYear : Int!
	var commenders : [Commender]!
	var fuelType : String!
	var fuelTypeColor : String!
	var fuelTypeId : Int!
	var id : Int!
	var plateImage : String!
	var trusted : Int!
	var vehicleImage : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required  init(_ json: JSON!){
		if json.isEmpty{
			return
		}
		balance = json["balance"].floatValue
		carBrand = json["car_brand"].stringValue
		carBrandId = json["car_brand_id"].intValue
		carModel = json["car_model"].stringValue
		carModelId = json["car_model_id"].intValue
		carPlateLettersAr = json["car_plate_letters_ar"].stringValue
		carPlateLettersEn = json["car_plate_letters_en"].stringValue
		carPlateNumbersAr = json["car_plate_numbers_ar"].stringValue
		carPlateNumbersEn = json["car_plate_numbers_en"].stringValue
		carType = json["car_type"].stringValue
		carTypeId = json["car_type_id"].intValue
		carYear = json["car_year"].intValue
		commenders = [Commender]()
		let commendersArray = json["delegates"].arrayValue
		for commendersJson in commendersArray{
			let value = Commender(_ : commendersJson)
			commenders.append(value)
		}
		fuelType = json["fuel_type"].stringValue
		fuelTypeColor = json["fuel_type_color"].stringValue
		fuelTypeId = json["fuel_type_id"].intValue
		id = json["id"].intValue
		plateImage = json["plate_image"].stringValue
		trusted = json["trusted"].intValue
		vehicleImage = json["vehicle_image"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if balance != nil{
			dictionary["balance"] = balance
		}
		if carBrand != nil{
			dictionary["car_brand"] = carBrand
		}
		if carBrandId != nil{
			dictionary["car_brand_id"] = carBrandId
		}
		if carModel != nil{
			dictionary["car_model"] = carModel
		}
		if carModelId != nil{
			dictionary["car_model_id"] = carModelId
		}
		if carPlateLettersAr != nil{
			dictionary["car_plate_letters_ar"] = carPlateLettersAr
		}
		if carPlateLettersEn != nil{
			dictionary["car_plate_letters_en"] = carPlateLettersEn
		}
		if carPlateNumbersAr != nil{
			dictionary["car_plate_numbers_ar"] = carPlateNumbersAr
		}
		if carPlateNumbersEn != nil{
			dictionary["car_plate_numbers_en"] = carPlateNumbersEn
		}
		if carType != nil{
			dictionary["car_type"] = carType
		}
		if carTypeId != nil{
			dictionary["car_type_id"] = carTypeId
		}
		if carYear != nil{
			dictionary["car_year"] = carYear
		}
		if commenders != nil{
			var dictionaryElements = [[String:Any]]()
			for commendersElement in commenders {
				dictionaryElements.append(commendersElement.toDictionary())
			}
			dictionary["delegates"] = dictionaryElements
		}
		if fuelType != nil{
			dictionary["fuel_type"] = fuelType
		}
		if fuelTypeColor != nil{
			dictionary["fuel_type_color"] = fuelTypeColor
		}
		if fuelTypeId != nil{
			dictionary["fuel_type_id"] = fuelTypeId
		}
		if id != nil{
			dictionary["id"] = id
		}
		if plateImage != nil{
			dictionary["plate_image"] = plateImage
		}
		if trusted != nil{
			dictionary["trusted"] = trusted
		}
		if vehicleImage != nil{
			dictionary["vehicle_image"] = vehicleImage
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         balance = aDecoder.decodeObject(forKey: "balance") as? Float
         carBrand = aDecoder.decodeObject(forKey: "car_brand") as? String
         carBrandId = aDecoder.decodeObject(forKey: "car_brand_id") as? Int
         carModel = aDecoder.decodeObject(forKey: "car_model") as? String
         carModelId = aDecoder.decodeObject(forKey: "car_model_id") as? Int
         carPlateLettersAr = aDecoder.decodeObject(forKey: "car_plate_letters_ar") as? String
         carPlateLettersEn = aDecoder.decodeObject(forKey: "car_plate_letters_en") as? String
         carPlateNumbersAr = aDecoder.decodeObject(forKey: "car_plate_numbers_ar") as? String
         carPlateNumbersEn = aDecoder.decodeObject(forKey: "car_plate_numbers_en") as? String
         carType = aDecoder.decodeObject(forKey: "car_type") as? String
         carTypeId = aDecoder.decodeObject(forKey: "car_type_id") as? Int
         carYear = aDecoder.decodeObject(forKey: "car_year") as? Int
         commenders = aDecoder.decodeObject(forKey: "delegates") as? [Commender]
         fuelType = aDecoder.decodeObject(forKey: "fuel_type") as? String
         fuelTypeColor = aDecoder.decodeObject(forKey: "fuel_type_color") as? String
         fuelTypeId = aDecoder.decodeObject(forKey: "fuel_type_id") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         plateImage = aDecoder.decodeObject(forKey: "plate_image") as? String
         trusted = aDecoder.decodeObject(forKey: "trusted") as? Int
         vehicleImage = aDecoder.decodeObject(forKey: "vehicle_image") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if balance != nil{
			aCoder.encode(balance, forKey: "balance")
		}
		if carBrand != nil{
			aCoder.encode(carBrand, forKey: "car_brand")
		}
		if carBrandId != nil{
			aCoder.encode(carBrandId, forKey: "car_brand_id")
		}
		if carModel != nil{
			aCoder.encode(carModel, forKey: "car_model")
		}
		if carModelId != nil{
			aCoder.encode(carModelId, forKey: "car_model_id")
		}
		if carPlateLettersAr != nil{
			aCoder.encode(carPlateLettersAr, forKey: "car_plate_letters_ar")
		}
		if carPlateLettersEn != nil{
			aCoder.encode(carPlateLettersEn, forKey: "car_plate_letters_en")
		}
		if carPlateNumbersAr != nil{
			aCoder.encode(carPlateNumbersAr, forKey: "car_plate_numbers_ar")
		}
		if carPlateNumbersEn != nil{
			aCoder.encode(carPlateNumbersEn, forKey: "car_plate_numbers_en")
		}
		if carType != nil{
			aCoder.encode(carType, forKey: "car_type")
		}
		if carTypeId != nil{
			aCoder.encode(carTypeId, forKey: "car_type_id")
		}
		if carYear != nil{
			aCoder.encode(carYear, forKey: "car_year")
		}
		if commenders != nil{
			aCoder.encode(commenders, forKey: "delegates")
		}
		if fuelType != nil{
			aCoder.encode(fuelType, forKey: "fuel_type")
		}
		if fuelTypeColor != nil{
			aCoder.encode(fuelTypeColor, forKey: "fuel_type_color")
		}
		if fuelTypeId != nil{
			aCoder.encode(fuelTypeId, forKey: "fuel_type_id")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if plateImage != nil{
			aCoder.encode(plateImage, forKey: "plate_image")
		}
		if trusted != nil{
			aCoder.encode(trusted, forKey: "trusted")
		}
		if vehicleImage != nil{
			aCoder.encode(vehicleImage, forKey: "vehicle_image")
		}

	}

}
