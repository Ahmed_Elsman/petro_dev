//
//    User.swift
//
//    Create by ahmed elsman on 11/7/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
import CoreData

class User : NSObject,NSCoding{
    
    var address : String!
    var balance : String!
    var email : String!
    var id : Int!
    var image : String!
    var lat : Int!
    var lng : Int!
    var mobile : String!
    var name : String!
    var code : String!
    var username : String!
    var vehicle : SingleCar!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required  init(_ json: JSON) {
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        balance = json["balance"].stringValue
        email = json["email"].stringValue
        id = json["id"].intValue
        image = json["image"].stringValue
        lat = json["lat"].intValue
        lng = json["lng"].intValue
        mobile = json["mobile"].stringValue
        name = json["name"].stringValue
        code = json["code"].stringValue
        username = json["username"].stringValue
        let vehicleJson = json["vehicle"]
        if !vehicleJson.isEmpty{
            vehicle = SingleCar(_ : vehicleJson)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if balance != nil{
            dictionary["balance"] = balance
        }
        if email != nil{
            dictionary["email"] = email
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if lat != nil{
            dictionary["lat"] = lat
        }
        if lng != nil{
            dictionary["lng"] = lng
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if name != nil{
            dictionary["name"] = name
        }
        if code != nil{
            dictionary["code"] = code
        }
        if username != nil{
            dictionary["username"] = username
        }
        if vehicle != nil{
            dictionary["vehicle"] = vehicle.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        balance = aDecoder.decodeObject(forKey: "balance") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        lat = aDecoder.decodeObject(forKey: "lat") as? Int
        lng = aDecoder.decodeObject(forKey: "lng") as? Int
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        code = aDecoder.decodeObject(forKey: "code") as? String
        username = aDecoder.decodeObject(forKey: "username") as? String
        vehicle = aDecoder.decodeObject(forKey: "vehicle") as? SingleCar
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if balance != nil{
            aCoder.encode(balance, forKey: "balance")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if lat != nil{
            aCoder.encode(lat, forKey: "lat")
        }
        if lng != nil{
            aCoder.encode(lng, forKey: "lng")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if code != nil{
            aCoder.encode(code, forKey: "code")
        }
        if username != nil{
            aCoder.encode(username, forKey: "username")
        }
        if vehicle != nil{
            aCoder.encode(vehicle, forKey: "vehicle")
        }
    }
    
}

