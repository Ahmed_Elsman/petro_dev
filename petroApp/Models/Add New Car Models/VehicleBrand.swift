//
//    VehicleBrand.swift
//
//    Create by ahmed elsman on 14/9/2018
//    Copyright © 2018. All rights reserved.

import Foundation
import SwiftyJSON


class VehicleBrand : NSObject{
    
    var id : Int!
    var models : [FuelType]!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required  init(_ json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        models = [FuelType]()
        let modelsArray = json["models"].arrayValue
        for modelsJson in modelsArray{
            let value = FuelType(_ : modelsJson)
            models.append(value)
        }
        title = json["title"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if models != nil{
            var dictionaryElements = [[String:Any]]()
            for modelsElement in models {
                dictionaryElements.append(modelsElement.toDictionary())
            }
            dictionary["models"] = dictionaryElements
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        models = aDecoder.decodeObject(forKey: "models") as? [FuelType]
        title = aDecoder.decodeObject(forKey: "title") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if models != nil{
            aCoder.encode(models, forKey: "models")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        
    }
    
}
