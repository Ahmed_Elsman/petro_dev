//
//  AppDelegate.swift
//  petroApp
//
//  Created by Ahmed Elsman on 7/2/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import EasyLocalization
import Google
import GoogleSignIn
import FBSDKLoginKit
import SwiftyJSON
import UserNotifications
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import SideMenu
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application( _ application: UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {   
        // Override point for customization after application launch.
        EasyLocalization.config(
            locale: .en,
            languageDictionary: [
                .ar : arabicArray,
                .en : englishArray,
                ]
        )
        IQKeyboardManager.shared.enable = true
        // add hockey app config.
        MSAppCenter.start("9d26c954-cb7e-4db1-9e71-70db7d997eb7", withServices:[
            MSAnalytics.self,
            MSCrashes.self
            ])
        checkLogin()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        let granted = PeopleStore.shared.updateDatabase()
        if granted
        {
            print("\(granted) people updated successfully")
        }
        else
        {
            print("error while updating database......")
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let granted = PeopleStore.shared.updateDatabase()
        
        if granted
        {
            print("\(granted) people updated successfully")
        }
        else
        {
            print("error while updating database......")
        }
    }

    func checkLogin() {

        if(UserUtil.loadUser()?.email != "" && UserUtil.loadUser() != nil && UserUtil.loadUserType() != "Delegate"){
            let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVCID") as! HomeVC
            let navigationController = EventsNavigationController(rootViewController: vc)
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.fade
            self.window?.layer.add(transition, forKey: kCATransition)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }else if(UserUtil.loadUser()?.username != "" && UserUtil.loadUser() != nil && UserUtil.loadUserType() != "User"){
            let vc =  UIStoryboard(name: "VehicheHome", bundle: nil).instantiateViewController(withIdentifier: "VehicleHomeVCID") as! VehicleHomeVC
            let navigationController = EventsNavigationController(rootViewController: vc)
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.fade
            self.window?.layer.add(transition, forKey: kCATransition)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }else{
            let vc = UIStoryboard(name: "auth", bundle: nil).instantiateViewController(withIdentifier: "LanguageSelectVC") as! LanguageSelectVC
            let navigationController = EventsNavigationController(rootViewController: vc)
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.fade
            window?.layer.add(transition, forKey: kCATransition)
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        }
    }
    
    func prepareAppFont(_ name:String) {
        UILabel.appearance().substituteFontName = name
        UIButton.appearance().substituteFontName = name
        UITextView.appearance().substituteFontName = name
        UITextField.appearance().substituteFontName = name
    }
    
    // MARK:- Send Request
    func sendRequest(){
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
        let lang =  Config.locale == .ar ? "ar" : "en"
        let param:[String: Any] = ["username":UserUtil.loadUser()?.email ?? "",
                                   "password":UserUtil.loadUserPassword() ?? "",
                                   "lang":lang,
                                   "type":"8"]
        
        Requests.instance.request(link: "signin".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    UserUtil.saveUserToken(response.data!["data"])
                    UserUtil.saveUserPassword(UserUtil.loadUserPassword() ?? "")
                    let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVCID") as! HomeVC
                    let navigationController = EventsNavigationController(rootViewController: vc)
                    let transition = CATransition()
                    transition.duration = 0.5
                    transition.type = CATransitionType.fade
                    self.window?.layer.add(transition, forKey: kCATransition)
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                    self.getUserData()
                }else{
                }
            }
        }
    }
    
    // MARK:- get User Data
    func getUserData(){
        let param:[String: Any] = [:]
        
        Requests.instance.request(link: "profile".route, method: .get, parameters: param) { (response) in
            print(response)
            if response.haveError{
            }else{
                if response.data!["success"].bool == true {
                    print(response)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
//                    let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVCID") as! HomeVC
//                    let navigationController = EventsNavigationController(rootViewController: vc)
//                    let transition = CATransition()
//                    transition.duration = 0.5
//                    transition.type = kCATransitionFade
//                    self.window?.layer.add(transition, forKey: kCATransition)
//                    self.window?.rootViewController = navigationController
//                    self.window?.makeKeyAndVisible()
                }else{
                }
            }
        }
    }
}

extension Data {
    var hexString: String {
        return map { String(format: "%02.2hhx", arguments: [$0]) }.joined()
    }
}

