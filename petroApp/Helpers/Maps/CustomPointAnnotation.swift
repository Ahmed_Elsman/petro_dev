//
//  CustomPointAnnotation.swift
//  petroApp
//
//  Created by Ahmed Elsman on 10/4/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import UIKit
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    var pinCustomImageName:String!
}
