//
//  EventsButton.swift
//  Events
//
//  Created by Elsman on 1/30/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import Foundation
import UIKit
import Material
class EventsButton: FlatButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 15
//        fromGradientWithDirection(.leftToRight, frame: self.frame, cornerRadius: 8)
        self.backgroundColor = UIColor.fromGradientWithDirection(.leftToRight, frame: self.frame, colors: [ Color.blueColor,Color.greenForGradiantColor], cornerRadius: 15)
        self.layer.masksToBounds = false
        self.layer.shadowColor = Color.grayColor.cgColor
        self.layer.shadowOffset = CGSize(width:0 , height:4)
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 8
        self.setTitleColor(.white, for: .normal)
    }

}

