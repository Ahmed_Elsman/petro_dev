//
//  NormalEventsTextField.swift
//  Events
//
//  Created by Elsman on 1/30/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import Foundation
import Material
import EasyLocalization

class NormalEventsTextField: UITextField {
    var rightIcon:UIImage = UIImage()
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.textAlignment = EasyLocalization.getLanguage() == .ar ? .right : .left
        self.textColor = .black
        self.shadow = true
        self.shadowColor = Color.grayColor
        self.backgroundColor = UIColor.white
        self.cornerRadius = 12
        self.borderStyle = .none
        self.setIcon(rightIcon)
    }
    
    func whiteField() {
        self.textColor = UIColor(hex: "EBEBEB")
        self.backgroundColor = UIColor.white
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "",attributes: [NSAttributedString.Key.foregroundColor: Color.grayColor])

    }
    
    func complainTextField() {
//        self.textColor = Color.grayColor
//        self.backgroundColor = UIColor.white
//        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "",attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    func setIcon(_ icon:UIImage ,_ iconFrame:CGRect = CGRect(x:0,y:0,width:10,height:10)) {
        self.leftView = nil
        self.leftViewMode = .never
        self.rightView = nil
        self.rightViewMode = .never
        let view = UIView(frame: CGRect(x:0,y:0,width:10,height:10))
        let imageView = UIImageView()
        imageView.frame = iconFrame
        imageView.image = icon
        view.addSubview(imageView)
        if EasyLocalization.getLanguage() == .en {
            self.leftView = view
            self.leftViewMode = .always
        }
        else {
            self.rightView = view
            self.rightViewMode = .always
        }
    }

    @IBInspectable var icon: UIImage? {
        set {
            self.rightIcon = newValue ?? UIImage()
        }
        get {
            return self.rightIcon
        }
    }
}


