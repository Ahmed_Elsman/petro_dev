//
//  EventsTextView.swift
//  events
//
//  Created by Elsman on 2/26/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import Foundation
import Material
import EasyLocalization

@IBDesignable
class EventsTextView : TextView {
    
    override func prepare() {
        super.prepare()
        self.textContainerInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4);
        self.placeholderColor = Color.grayColor

    }
    
    
    @IBInspectable var localizeKeyPlaceholder: String? {
        set {
            // set new value from dictionary
            DispatchQueue.main.async{
                self.placeholder = newValue?.locale
                self.textAlignment = rtlLang.contains(language ?? .en) ? .right : .left
            }
        }
        get {
            return self.placeholder
        }
    }
}
