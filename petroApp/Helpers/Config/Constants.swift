//
//  Constants.swift
//  Events
//
//  Created by Elsman on 1/30/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import Foundation
import UIKit
import EasyLocalization
//MARK:- Base configrations
enum Config
{
    static let mainUrl = "http://app.petroapp.com.sa/"
    static let baseUrl = "http://app.petroapp.com.sa/"
    static let apiUrl = baseUrl + "api/"
    static let imageUrl = "http://app.petroapp.com.sa/"
    static let locale:locale = EasyLocalization.getLanguage()
    static let Present_Vehicle_Details = "VehicleDetails"
    static let Present_Petro_Sites = "GoPetroSites"
    static let Present_Add_New_Car = "GoAddNewCar"
    static let Present_My_Profile = "GoMyProfile"
    static let Present_My_Bills = "GoMyBills"
    static let Present_My_Bills_Details = "GoMyBillsDetails"
    static let Present_My_Reports = "GoMyReports"
    static let Present_SendـAcomplaint = "GoSendAcomplaint"
    static let Present_Contact_Us = "GoContactUs"
    static let Present_My_Profile_Details = "GoMyProfileDetails"
    static let Present_Plat_View = "GoPlatView"

    static let myMessageColor = UIColor.lightGray
    static let friendMessageColor = Color.greenForGradiantColor
    static var user_id: Int = 0
}

enum Color {
    
    static let greenColor =  UIColor.hex("ff363e")//14cc9d
    static let greenForGradiantColor = UIColor.hex("ff363e")//00ADB9
    static let blueColor = UIColor(hex:"692569")//027ac1
    static let blueForGradiantColor = UIColor(hex:"692569")//008CC3
    static let grayColor = UIColor(hex:"bfbfbf")
    static let blakColor = UIColor(hex:"c1212323")
    
    static let newBlueColor = UIColor(hex:"056fdd")
    static let newGrayColor = UIColor(hex:"6a6a6a")
    static let newGreenColor = UIColor(hex:"a1aab3")
    static let newLightGrayColor = UIColor(hex:"f9f9f9")
}
