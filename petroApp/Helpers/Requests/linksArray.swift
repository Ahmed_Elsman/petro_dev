//
//  linksArray.swift
//  Events
//
//  Created by Elsman on 1/30/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import Foundation
var linksArray = [
    "signin":"login",
    "signup":"register",
    "profile" : "primaryuser_profile",
    "getVehicles" : "vehicles?lang=",
    "getVehicle" : "vehicles/",
    "getModels":"vehicle_models",
    "checkVehicles":"vehicles",
    "getBills":"primaryuser_bills",
    "getVehiclesReports":"search_vehicles_reports",
    "editProfile":"primaryuser_edit_profile",
    "settings":"settings",
    "send_complaint":"send_complaint",
    "getPetroLocations":"petro_locations",
    "changePassword":"primaryuser_change_password",
    "sendActivation":"password/reset",
    "updatePassword":"password/update",
    "addBalance":"add_vehicle_balance",
    "deleteBalance":"delete_vehicle_balance",
    "regenerateCode":"regenerate_code",
    "requestBalance":"balance_request",
    "editDelegate":"delegates",
    "bankList":"bank_account",
    "pages":"pages?lang=",
    //delegate api
    "delegate_profile":"delegate_profile",
    "getDelegateBills":"get_delegate_bills",
    "getVehicleReport":"get_vehicle_report",
]
