//
//  Routes.swift
//  Events
//
//  Created by Elsman on 1/30/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import Foundation

/*
 *  Routes to return Links
 */

extension String {
    var route: String {
        get {
            if let link = linksArray[self]{
                let lang =  Config.locale == .ar ? "ar" : "en"
//                return Config.apiUrl + link + "?locale=" + lang + "&"
                return Config.apiUrl + link
            }else{
                return Config.baseUrl
            }
        }
        
    }
}

