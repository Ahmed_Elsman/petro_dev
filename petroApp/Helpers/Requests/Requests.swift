//
//  Requests.swift
//  Events
//
//  Created by Elsman on 1/30/18.
//  Copyright © 2018 Elsman. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Requests {
    
    static var instance: Requests {
        return Requests()
    }
    
    func request(link:String , method:HTTPMethod ,parameters:[String: Any] = [:] , completion: @escaping (ReturnRequest) -> ()) {
        var headers = ["Accept": "application/json"]
        if(UserUtil.loadUserToken() != nil){
            headers = ["Content-Type":"application/x-www-form-urlencoded","Accept": "application/json","Authorization": "Bearer "+UserUtil.loadUserToken()!]
        }else if (link == "http://app.petroapp.com.sa/api/pages?lang=en&category=ios_individual" || link == "http://app.petroapp.com.sa/api/pages?lang=ar&category=ios_individual"){
            headers = ["Content-Type":"application/x-www-form-urlencoded","Accept": "application/json","Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzY4NzQ1NzIsInN1YiI6MTkwLCJpc3MiOiJodHRwOi8vcHJlLnBldHJvYXBwLmNvbS5zYS9hcGkvbG9naW4iLCJpYXQiOjE1NDUzMzg1NzIsIm5iZiI6MTU0NTMzODU3MiwianRpIjoiUEJ0c0hySk81NkRjQUFqUSJ9.zJ7_o8UK83fSjCBq9-q6IpinzH7Avo9deNAmp9gtqTQ"]
        }
        Alamofire.request(link, method: method, parameters:parameters, headers: headers).responseJSON { response in
            self.result(response, completion: { (data) in
                completion(data)
            })
        }
    }
    
    func requestMultipart(link:String , method:HTTPMethod ,parameters:[String: Any] = [:] , completion: @escaping (ReturnRequest) -> ()) {
        Alamofire.upload(multipartFormData: { multipartFormData in
            // parametiers
            parameters.forEach({ (key,value) in
                if key == "image"{
                    multipartFormData.append(((value as! UIImage).jpegData(compressionQuality: 1.0) as Data?)!, withName: "image", fileName: "UserImage.jpg", mimeType: "image/jpg")
                }else{
                    multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                }
            })
        }, to: link, method: .post , headers: ["Accept": "application/json" ], encodingCompletion: { result in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress { progress in // main queue by default
                    // Start the upload
                    print("Upload Progress: \(progress.fractionCompleted)")
                }
                upload.responseJSON { response in
                    self.result(response, completion: { (data) in
                        completion(data)
                    })
                }
            case .failure(let encodingError):
                completion(ReturnRequest(haveError : true, error : encodingError.localizedDescription))
                break
            }
        })
        
    }
    
    func result(_ response: DataResponse<Any>, completion: @escaping (ReturnRequest) -> ()){
        switch response.result {
        case .success:
            if let data = response.result.value{
                print(data)
                let json = JSON(data)
                if json["error"].string == "Unauthenticated."{
                    completion(ReturnRequest(unauthenticated : true))
                }else{
                    completion(ReturnRequest(json))
                }
            }else{
                completion(ReturnRequest(haveError : true))
            }
        case .failure(let encodingError):
            completion(ReturnRequest(haveError : true, error : encodingError.localizedDescription))
            break
        }
    }
    
}
